const jwt = require('jsonwebtoken');
const Role = require('../models/role');
const Acc = require('../models/account');

function auth(req, res, next) {
    const str = req.header('x-auth-token');
    const token = str.slice(0,30) + str.slice(30,50).split("").reverse().join("") + str.slice(50);
    if (!token) return res.status(401).json({ Message: "Vui lòng tiến hành đăng nhập trước khi có thể sử dụng chức năng này!" });
    try {
        const decoded = jwt.verify(token, process.env.JWT);
        req.user = decoded;
        role = decoded.role;

        Acc.findOne({email: decoded.email}).then(acc => {
            if (acc.isLogin === 0 || Date.now() > decoded.expires) {
                acc.isLogin = 0;
                acc.save();
                return res.status(404).json({ Message: "Vui lòng tiến hành đăng nhập lại!" });
            } else {
                Role.findOne({ idRo: role }).then(role => {
                    var findFunc = role.funcs.find(function(func){
                        return func == "qlnp";
                    });
                    if (findFunc === "qlnp") next();
                    else return res.status(404).json({ Warning: "Bạn không có quyền truy cập chức năng này!" });
                });
            }
        });
    } catch(e) {
        return res.status(404).json({ Message: "Vui lòng tiến hành đăng nhập lại!" });
    }
}

module.exports = auth;