const jwt = require('jsonwebtoken');
const Acc = require('../models/account');

function auth(req, res, next) {
    const str = req.header('x-auth-token');
    const token = str.slice(0,30) + str.slice(30,50).split("").reverse().join("") + str.slice(50);
    console.log("token1" + str);
    console.log("token2" + token);
    if (!token) return res.status(401).json({ Message: "Vui lòng tiến hành đăng nhập trước khi có thể sử dụng chức năng này!" });
    try {
        const decoded = jwt.verify(token, process.env.JWT);
        req.user = decoded;
        Acc.findOne({email: decoded.email}).then(acc => {
            if (Date.now() > decoded.expires) {
                acc.isLogin = 0;
                acc.save();
                return res.status(404).json({ Message: "Vui lòng tiến hành đăng nhập lại 123!" });
            } else next();
        });
    } catch (e) {
        return res.status(404).json({ Message: "Vui lòng tiến hành đăng nhập lại abc!" });
    }
}

module.exports = auth;