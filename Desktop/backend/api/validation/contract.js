const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateContrInput(data) {
    let errors = {};
    // Convert empty fields to an empty string so we can use validator functions
    data.type = !isEmpty(data.type) ? data.type : "";
    data.idContr = !isEmpty(data.idContr) ? data.idContr : "";
    data.startDate = !isEmpty(data.startDate) ? data.startDate : "";
    data.endDate = !isEmpty(data.endDate) ? data.endDate : "";
    data.workingH = !isEmpty(data.workingH) ? data.workingH : "";
    data.baseSala = !isEmpty(data.baseSala) ? data.baseSala : "";
    data.allowance = !isEmpty(data.allowance) ? data.allowance : "";
    data.payDate = !isEmpty(data.payDate) ? data.payDate : "";
    data.payForms = !isEmpty(data.payForms) ? data.payForms : "";
    data.insurance = !isEmpty(data.insurance) ? data.insurance : "";


    // ID department check
    if (Validator.isEmpty(data.idContr)) {
        errors.idContr = "Nhập mã hợp đồng!";
      }
    // Name checks
    if (Validator.isEmpty(data.type)) {
        errors.type = "Nhập tên hợp đồng!";
    }
    if (Validator.isEmpty(data.startDate)) {
        errors.startDate = "Nhập tên ngày bắt đầu có hiệu lực của hợp đồng!";
    }
    if (Validator.isEmpty(data.endDate)) {
        errors.endDate = "Nhập tên ngày hết hiệu lực của hợp đồng!";
    }
    if (Validator.isEmpty(data.workingH)) {
        errors.workingH = "Nhập thời gian làm việc!";
    }
    if (Validator.isEmpty(String(data.baseSala))) {
        errors.baseSala = "Nhập lương cơ bản!";
    }
    if (Validator.isEmpty(String(data.allowance))) {
        errors.allowance = "Nhập phụ cấp!";
    }
    if (Validator.isEmpty(data.payDate)) {
        errors.payDate = "Nhập ngày trả lương!";
    }
    if (Validator.isEmpty(data.payForms)) {
        errors.payForms = "Nhập hình thức trả lương!";
    }
    if (Validator.isEmpty(data.insurance)) {
        errors.insurance = "Nhập bảo hiểm!";
    }
    return {
        errors,
        isValid: isEmpty(errors)
    };
};