const Validator = require("validator");
const isEmpty = require("is-empty");
const passwordValidator = require("password-validator");

const pass1 = new passwordValidator();
const pass2 = new passwordValidator();

pass1
.has().uppercase()                              // Must have uppercase letters
.has().lowercase()                              // Must have lowercase letters
.has().digits()                                 // Must have digits
.has().not().spaces()                           // Should not have spaces

pass2
.is().not().oneOf(['Passw0rd', 'Password123', 'Password1', 'Password2', 'Password1234']); // Blacklist these values
module.exports = function validatePasswordInput(data) {
    let errors1 = {};
    // Convert empty fields to an empty string so we can use validator functions
    data.password = !isEmpty(data.password) ? data.password : "";
    data.password2 = !isEmpty(data.password2) ? data.password2 : "";

    // Password checks
    if (Validator.isEmpty(data.password)) {
        errors1.Warning = "Vui lòng điền password";
    }
    if (Validator.isEmpty(data.password2)) {
        errors1.Warning = "Vui lòng điền passowrd xác thực!";
    }
    if (!Validator.isLength(data.password, { min: 8, max: 100 })) {
        errors1.Warning = "Độ dài cần lớn hơn 8 ký tự!";
    }
    if (!pass1.validate(data.password)) {
        errors1.Warning = "Password phải bao gồm ký tự thường, in hoa, chữ số và không chưa khoảng trắng!";
    }

    if (!pass2.validate(data.password)) {
        errors1.Warning = "Password quá đơn giản, vui lòng chọn password phức tạp hơn!";
    }

    if (!Validator.equals(data.password, data.password2)) {
        errors1.password2 = "Password chưa trùng khớp!";
    }
    return {
        errors1,
        isValid1: isEmpty(errors1)
    };
};