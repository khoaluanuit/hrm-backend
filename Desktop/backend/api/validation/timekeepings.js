const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateTkpInput(data) {
    let errors = {};
    // Convert empty fields to an empty string so we can use validator functions
    data.month = !isEmpty(data.month) ? data.month : "";
    data.year = !isEmpty(data.year) ? data.year : "";

    if (Validator.isEmpty(data.month)) {
        errors.month = "Vui lòng chọn tháng!";
    }

    if (data.month < 1 || data.month > 12){
        errors.month = "Vui lòng chọn lại tháng!";
    }

    if (Validator.isEmpty(data.year)) {
        errors.year = "Vui lòng chọn năm!";
    }
    
    if (data.year < 2019 || data.year > 100000){
        errors.year = "Vui lòng chọn lại năm!";
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
};