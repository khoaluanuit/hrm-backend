const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateRoleInput(data) {
    let errors = {};
    // Convert empty fields to an empty string so we can use validator functions
    data.name = !isEmpty(data.name) ? data.name : "";
    data.idRo = !isEmpty(data.idRo) ? data.idRo : "";

    // ID department check
    if (Validator.isEmpty(data.idRo)) {
        errors.idRo = "Nhập mã vai trò!";
      }
    // Name checks
    if (Validator.isEmpty(data.name)) {
        errors.name = "Nhập tên vai trò!";
    }
    if (isEmpty(data.funcs)) {
        errors.funcs = "Vui lòng chọn các chức năng cấp cho vai trò!";
      }
    return {
        errors,
        isValid: isEmpty(errors)
    };
};