const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateRegisterInput(data) {
  let errors = {};
// Convert empty fields to an empty string so we can use validator functions
  data.username = !isEmpty(data.username) ? data.username : "";
  data.email = !isEmpty(data.email) ? data.email : "";
// Name checks
  if (Validator.isEmpty(data.username)) {
    errors.username = "Vui lòng điền username!";
  }
// Email checks
  if (Validator.isEmpty(data.email)) {
    errors.email = "Vui lòng điền email!";
  } else if (!Validator.isEmail(data.email)) {
    errors.email = "Email chưa đúng!";
  }
return {
    errors,
    isValid: isEmpty(errors)
  };
};