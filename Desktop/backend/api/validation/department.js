const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateDepartInput(data) {
    let errors = {};
    // Convert empty fields to an empty string so we can use validator functions
    data.name = !isEmpty(data.name) ? data.name : "";
    data.email = !isEmpty(data.email) ? data.email : "being updated";
    data.idDpmt = !isEmpty(data.idDpmt) ? data.idDpmt : "";
    data.manager = !isEmpty(data.manager) ? data.manager : "being updated!";

    // ID department check
    if (Validator.isEmpty(data.idDpmt)) {
        errors.idDpmt = "Nhập mã phòng ban!";
      }
    // Name checks
    if (Validator.isEmpty(data.name)) {
        errors.Warning = "Nhập tên phòng ban!";
    }
    // Email checks
    if (!Validator.isEmpty(data.email) && data.email != "being updated") {
        if (!Validator.isEmail(data.email)) {
            errors.email = "Email chưa đúng!";
        }
    }
    return {
        errors,
        isValid: isEmpty(errors)
    };
};