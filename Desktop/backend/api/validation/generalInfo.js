const isEmpty = require("is-empty");
module.exports = function validateGenInput(data) {
    let errors = {};

    if (!isEmpty(data.socialInsuC) && !Number.isInteger(parseInt(data.socialInsuC))) {
        errors.socialInsuC = "Tỉ lệ đóng bảo hiểm xã hội, bảo hiểm tai nạn cho công ty phải là một số thực!";
    }
    if (!isEmpty(data.socialInsuE) && !Number.isInteger(parseInt(data.socialInsuE))) {
        errors.socialInsuE = "Tỉ lệ đóng bảo hiểm xã hội cho nhân viên phải là một số thực!";
    }
    if (!isEmpty(data.healthInsuC) && !Number.isInteger(parseInt(data.healthInsuC))) {
        errors.healthInsuC = "Tỉ lệ đóng bảo hiểm y tế cho công ty phải là một số thực!";
    }
    if (!isEmpty(data.healthInsuE) &&  !Number.isInteger(parseInt(data.healthInsuE))) {
        errors.healthInsuE = "Tỉ lệ đóng bảo hiểm y tế cho nhân viên phải là một số thực!";
    }
    if (!isEmpty(data.unempInsuC) && !Number.isInteger(parseInt(data.unempInsuC))) {
        errors.unempInsuC = "Tỉ lệ đóng bảo hiểm thất nghiệp cho công ty phải là một số thực!";
    }
    if (!isEmpty(data.unempInsuE) && !Number.isInteger(parseInt(data.unempInsuE))) {
        errors.unempInsuE = "Tỉ lệ đóng bảo hiểm thất nghiệp cho nhân viên phải là một số thực!";
    }
    if (!isEmpty(data.lateEarly) && !Number.isInteger(parseInt(data.lateEarly))) {
        errors.lateEarly = "Tiền phạt làm thiếu giờ phải là một số thực!";
    }
    if (!isEmpty(data.ovtHour) && !Number.isInteger(parseInt(data.ovtHour))) {
        errors.ovtHour = "Tiền làm thêm giờ phải là một số thực!";
    }
    if (!isEmpty(data.ovtHourInDateOff) && !Number.isInteger(parseInt(data.ovtHourInDateOff))) {
        errors.ovtHourInDateOff = "Tiền làm thêm giờ vào ngày nghỉ phải là một số thực!";
    }
    if (!isEmpty(data.ovtHourInPubHoli) && !Number.isInteger(parseInt(data.ovtHourInPubHoli))) {
        errors.ovtHourInPubHoli = "Tiền làm thêm giờ vào lễ tết phải là một số thực!";
    }
    if (!isEmpty(data.workInDateOff) && !Number.isInteger(parseInt(data.workInDateOff))) {
        errors.workInDateOff = "Tiền làm vào ngày nghỉ phải là một số thực!";
    }
    if (!isEmpty(data.nightShift) && !Number.isInteger(parseInt(data.nightShift))) {
        errors.nightShift = "Tiền làm ca đêm phải là một số thực!";
    }
    if (!isEmpty(data.nightShiftInDateOff) && !Number.isInteger(parseInt(data.nightShiftInDateOff))) {
        errors.nightShiftInDateOff = "Tiền làm ca đêm vào ngày nghỉ phải là một số thực!";
    }
    if (!isEmpty(data.nightShiftInPubHoli) && !Number.isInteger(parseInt(data.nightShiftInPubHoli))) {
        errors.nightShiftInPubHoli = "Tiền làm ca đêm vào lễ tết phải là một số thực!";
    }
    return {
        errors,
        isValid: isEmpty(errors)
    };
};