const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateSalaScalInput(data) {
    let errors = {};

    data.idSalaScal = !isEmpty(data.idSalaScal) ? data.idSalaScal : "";
    data.name = !isEmpty(data.name) ? data.name : "";

    if (Validator.isEmpty(data.idSalaScal)) {
        errors.idSalaScal = "Vui lòng điền mã bậc lương!";
    } 
    if (Validator.isEmpty(data.name)) {
        errors.name = "Tên của bậc lương không được để trống!";
    }
    if (isEmpty(data.salary)) {
        errors.salary = "Không được để trống tiền lương!";
    } else if (!Number.isInteger(parseInt(data.salary))) {
        errors.salary = "Tiền lương phải là một số thực!";
    }
    if (!isEmpty(data.allowance) &&  !Number.isInteger(parseInt(data.allowance))) {
        errors.allowance = "Tiền phụ cấp phải là một số thực!";
    }
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
};