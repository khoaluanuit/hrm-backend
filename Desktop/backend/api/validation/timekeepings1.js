const isEmpty = require("is-empty");
module.exports = function validateTkp1Input(data) {
    let errors1 = {};

    if (!isEmpty(data.pubHoli) && !Number.isInteger(parseInt(data.pubHoli))) {
        errors1.pubHoli = "Tỉ lệ đóng bảo hiểm xã hội, bảo hiểm tai nạn cho công ty phải là một số nguyên!";
    }
    if (!isEmpty(data.annualLeave) && !Number.isInteger(parseInt(data.annualLeave))) {
        errors1.annualLeave = "Tỉ lệ đóng bảo hiểm xã hội cho nhân viên phải là một số nguyên!";
    }
    if (!isEmpty(data.leavePermit) && !Number.isInteger(parseInt(data.leavePermit))) {
        errors1.leavePermit = "Tỉ lệ đóng bảo hiểm y tế cho công ty phải là một số nguyên!";
    }
    if (!isEmpty(data.leaveNoPermit) &&  !Number.isInteger(parseInt(data.leaveNoPermit))) {
        errors1.leaveNoPermit = "Tỉ lệ đóng bảo hiểm y tế cho nhân viên phải là một số nguyên!";
    }
    if (!isEmpty(data.sickLeave) && !Number.isInteger(parseInt(data.sickLeave))) {
        errors1.sickLeave = "Tỉ lệ đóng bảo hiểm thất nghiệp cho công ty phải là một số nguyên!";
    }
    if (!isEmpty(data.mertanityLeave) && !Number.isInteger(parseInt(data.mertanityLeave))) {
        errors1.mertanityLeave = "Tỉ lệ đóng bảo hiểm thất nghiệp cho nhân viên phải là một số nguyên!";
    }
    if (!isEmpty(data.ovtHour) && !Number.isInteger(parseInt(data.ovtHour))) {
        errors1.ovtHour = "Tỉ lệ đóng bảo hiểm thất nghiệp cho nhân viên phải là một số nguyên!";
    }
    if (!isEmpty(data.ovtHourInDateOff) && !Number.isInteger(parseInt(data.ovtHourInDateOff))) {
        errors1.ovtHourInDateOff = "Tỉ lệ đóng bảo hiểm thất nghiệp cho nhân viên phải là một số nguyên!";
    }
    if (!isEmpty(data.ovtHourInPubHli) && !Number.isInteger(parseInt(data.ovtHourInPubHli))) {
        errors1.ovtHourInPubHli = "Tỉ lệ đóng bảo hiểm thất nghiệp cho nhân viên phải là một số nguyên!";
    }
    if (!isEmpty(data.workInDateOff) && !Number.isInteger(parseInt(data.workInDateOff))) {
        errors1.workInDateOff = "Tỉ lệ đóng bảo hiểm thất nghiệp cho nhân viên phải là một số nguyên!";
    }
    if (!isEmpty(data.workInPubHoli) && !Number.isInteger(parseInt(data.workInPubHoli))) {
        errors1.workInPubHoli = "Tỉ lệ đóng bảo hiểm thất nghiệp cho nhân viên phải là một số nguyên!";
    }
    if (!isEmpty(data.nightShift) && !Number.isInteger(parseInt(data.nightShift))) {
        errors1.nightShift = "Tỉ lệ đóng bảo hiểm thất nghiệp cho nhân viên phải là một số nguyên!";
    }
    if (!isEmpty(data.nightShiftInDateOff) && !Number.isInteger(parseInt(data.nightShiftInDateOff))) {
        errors1.nightShiftInDateOff = "Tỉ lệ đóng bảo hiểm thất nghiệp cho nhân viên phải là một số nguyên!";
    }
    if (!isEmpty(data.nightShiftInPubHoli) && !Number.isInteger(parseInt(data.nightShiftInPubHoli))) {
        errors1.nightShiftInPubHoli = "Tỉ lệ đóng bảo hiểm thất nghiệp cho nhân viên phải là một số nguyên!";
    }
    
    return {
        errors1,
        isValid1: isEmpty(errors1)
    };
};