const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateEmpInput(data) {
    let errors = {};
    // Convert empty fields to an empty string so we can use validator functions
    data.fullname = !isEmpty(data.fullname) ? data.fullname : "";
    data.idEmp = !isEmpty(data.idEmp) ? data.idEmp : "";
    data.email = !isEmpty(data.email) ? data.email : "";

    // ID department check
    if (Validator.isEmpty(data.idEmp)) {
        errors.idEmp = "Nhập mã nhân viên!";
      }
    // Name checks
    if (Validator.isEmpty(data.fullname)) {
        errors.name = "Nhập tên nhân viên!";
    }
    // Email checks
    if (!Validator.isEmpty(data.email)) {
        if (!Validator.isEmail(data.email)) {
            errors.email = "Em chưa đúng!";
        }
    }
    return {
        errors,
        isValid: isEmpty(errors)
    };
};