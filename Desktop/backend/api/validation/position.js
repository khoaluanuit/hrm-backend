const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validatePosiInput(data) {
    let errors = {};
    // Convert empty fields to an empty string so we can use validator functions
    data.name = !isEmpty(data.name) ? data.name : "";
    data.idPosi = !isEmpty(data.idPosi) ? data.idPosi : "";
    data.idDpmt = !isEmpty(data.idDpmt) ? data.idDpmt : "";

    // ID department check
    if (Validator.isEmpty(data.idPosi)) {
        errors.idPosi = "Nhập mã chức vụ!";
      }
    // Name checks
    if (Validator.isEmpty(data.name)) {
        errors.name = "Nhập tên phòng ban!";
    }
    if (Validator.isEmpty(data.idDpmt)) {
        errors.idDpmt = "Vui lòng chọn phòng ban!";
      }
    return {
        errors,
        isValid: isEmpty(errors)
    };
};