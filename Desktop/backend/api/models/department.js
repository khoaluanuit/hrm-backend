const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const DepartSchema = new Schema({
    idDpmt: { type: String, required: true, unique: true },
    name: { type: String, required: true},
    email: { type: String },
    manager: { type: String },
    dateCreated: { type: String },
    dateModified: { type: String }
});
module.exports = Depart = mongoose.model("departments", DepartSchema);