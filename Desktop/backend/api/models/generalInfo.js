const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const GenSchema = new Schema ({
    socialInsuC: {type: Number},
    socialInsuE: {type: Number},
    healthInsuC: {type: Number},
    healthInsuE: {type: Number},
    unempInsuC: {type: Number},
    unempInsuE: {type: Number},
    payDate: {type: String},
    payForms: {type: String},
    lateEarly: {type: Number},
    ovtHour: {type: Number},
    ovtHourInDateOff: {type: Number},
    ovtHourInPubHoli: {type: Number},
    workInDateOff: {type: Number},
    workInPubHoli: {type: Number},
    nightShift: {type: Number},
    nightShiftInDateOff: {type: Number},
    nightShiftInPubHoli: {type: Number},
    dateModified: {type: String}
});
module.exports = Gen = mongoose.model("generalInfo", GenSchema);