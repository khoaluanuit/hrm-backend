const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const funcsSchema = new Schema({
    idFunc: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String
    },
    description: {
        type: String
    }
});
module.exports = Func = mongoose.model("functions", funcsSchema);