const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const tkpSchema = new Schema({
    idEmp: {type: String, require: true},
    month: {type: String, required: true},
    year: {type: String, required: true},
    days: {type: Array, require: true},
    annualLeave: {type: Number},
    pubHoli: {type: Number},
    leavePermit: {type: Number},
    leaveNoPermit: {type: Number},
    sickLeave: {type: Number},
    mertanityLeave: {type: Number},
    ovtHour: {type: Number},
    ovtHourInDateOff: {type: Number},
    ovtHourInPubHli: {type: Number},
    workInDateOff: {type: Number},
    workInPubHoli: {type: Number},
    nightShift: {type: Number},
    nightShiftInDateOff: {type: Number},
    nightShiftInPubHoli: {type: Number},
});
module.exports = Tkp = mongoose.model("timekeepings", tkpSchema)