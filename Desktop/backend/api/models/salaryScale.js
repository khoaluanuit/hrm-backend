const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const salaScalSchema = new Schema({
    idSalaScal: {type: String, require: true, unique: true},
    name: {type: String, require: true},
    salary: {type: Number, require: true},
    allowance: {type: Number},
    dateCreate: {type: String},
    dateModified: {type: String}
});
module.exports = SalaScal = mongoose.model("salaryScals", salaScalSchema);