const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const roleSchema = new Schema({
    idRo: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String
    },
    funcs: {
        type: Array
    }
});
module.exports = Role = mongoose.model("roles", roleSchema)