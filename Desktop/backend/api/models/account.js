const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const AccSchema = new Schema({
  username: { type: String, required: true},
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  active: { type: Boolean, },
  role: {type: String},
  auth2: {type: Boolean},
  isLogin: {type: Number},
  lastOTP: {type: String},
  timeOTP: {type: Number},
  numberOfReset: {type: Number},
  numberOfLoginFail: {type: Number},
  timeLoginFail: {type: Number},

});
module.exports = Acc = mongoose.model("accounts", AccSchema);