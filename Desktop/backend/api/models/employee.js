const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const EmpSchema = new Schema({
    idEmp: {type: String, required: true, unique: true},
    fullname: {type: String, required: true },
    email: {type: String},
    account: {type: String, required: true},
    contract:{type: String},
    gender: {type: String},
    birthdate: {type: String},
    birthplace: {type: String},
    placeOfRes: {type: String},
    address: {type: String },
    phone: {type: Number},
    idCard: {type: Number},
    dateOfIssue: {type: String},
    placeOfIssue: {type: String},
    bSalarySca: {type: Number},
    religion: {type: String},
    position: {type: String},
    standard: {type: String},
    nationlaty: {type: String},
    maritalStt: {type: Number},
    department: {type: String},
    startDate: {type: String},
    image: {type: String},
    note: {type: String},
    dateCreated: {type: String},
    dateModified: {type: String}
});
module.exports = Emp = mongoose.model("employments", EmpSchema);