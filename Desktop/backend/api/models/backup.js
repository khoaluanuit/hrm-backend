const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const BackupSchema = new Schema({
  name: { type: String, required: true},
  dateCreate: { type: String}
});
module.exports = Backup = mongoose.model("backups", BackupSchema);