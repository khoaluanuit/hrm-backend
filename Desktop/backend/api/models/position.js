const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const PosiSchema = new Schema({
    idPosi: { type: String, required: true, unique: true },
    name: { type: String, required: true, unique: true},
    idDpmt: {type: String, required: true},
    dateCreated: { type: String },
    dateModified: { type: String }
});
module.exports = Posi = mongoose.model("positions", PosiSchema);