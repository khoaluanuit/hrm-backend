const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const payrollSchema = new Schema({
    idPr: {type: String, required: true, unique: true},
    idEmp: {type: String, require: true},
    month: {type: String, required: true},
    year: {type: String, required: true},
    bonus: {type: Number},
    bunish: {type: Number},
    isOn: {type: Boolean} 
});
module.exports = Pr = mongoose.model("payrolls", payrollSchema)