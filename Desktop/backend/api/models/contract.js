const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const ContrSchema = new Schema({
    idContr: { type: String, required: true, unique: true },
    type: { type: String, required: true},
    startDate: { type: String },
    endDate: { type: String },
    workingH: { type: String },
    baseSala: { type: Number },
    allowance: { type: Number },
    payDate: { type: String },
    payForms: { type: String },
    insurance: { type: String },
    note: { type: String },
    dateCreated: { type: String },
    dateModified: { type: String }
});
module.exports = Contr = mongoose.model("contracts", ContrSchema);