const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const isEmpty = require("is-empty");
const fs = require('fs');
const _ = require('lodash');
const exec = require('child_process').exec;
const path = require('path');


// Load backup model
const Backup = require("../models/backup");
const Acc = require("../models/account");

const authQlht1 = require("../middleware/auth-qlht1");
const authQlht = require("../middleware/auth-qlht");
// Load input validation

const backupDirPath = path.join(__dirname + '/../backup', 'database-backup');

const dbOptions = {
    user: 'linh',
    pass: '123456',
    host: '192.168.145.129',
    port: 27017,
    database: 'hrm2019',
    BackupPath: backupDirPath
};

// return stringDate as a date object.
exports.stringToDate = dateString => {
    return new Date(dateString);
};

router.post('/backup', authQlht1, (req, res) => {
    let date = new Date();

    // Current date
    currentDate = this.stringToDate(date);
    let newBackupDir =
        currentDate.getFullYear() +
        '-' +
        (currentDate.getMonth() + 1) +
        '-' +
        currentDate.getDate() +
        '-' +
        currentDate.getHours() +
        'h' +
        currentDate.getMinutes() +
        'p';

    console.log("database-backup-mongodump-" + newBackupDir);

    // New backup path for current backup process
    let newBackupPath = dbOptions.BackupPath + '-hrm2019-' + newBackupDir;
    // Command for mongodb dump process
    let cmd =
        'mongodump --host ' +
        dbOptions.host +
        ' --port ' +
        dbOptions.port +
        ' --db ' +
        dbOptions.database +
        ' --username ' +
        dbOptions.user +
        ' --password ' +
        dbOptions.pass +
        ' --out ' +
        newBackupPath;

    try {
        exec(cmd);
        var newBackup = new Backup({
            name: "database-backup-hrm2019-" + newBackupDir,
            dateCreate: currentDate
        });
        newBackup.save().then(bk => res.send("Backup successfully!"));
    } catch (e) {
        console.log(e);
    }
});

router.get('/', authQlht1,(req, res) => {
    Backup.find({}).then(bks => {
        if (bks.length < 1) res.status(200).json({ Message: "Chưa có bản backup dữ liệu nào!" });
        else res.send(bks);
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.get('/:id', authQlht1, (req, res) => {
    Backup.findById(req.params.id).then(bk => {
        if (!bk) res.status(404).json({ Error: "Không tìm thấy bản backup này!" });
        else res.send(bk);
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
router.delete('/delete/:id', authQlht, (req, res) => {
    Backup.findById(req.params.id).then((bk) => {
        if (!bk) res.status(404).json({ Error: "Không tìm thấy bản backup này!" });
        else {
            pa = path.join(__dirname + '/../backup', bk.name);
            if (fs.existsSync(pa)) {
                exec('rm -rf ' + pa);
                res.status(200).json({ Message: "Xóa thành công!" });
            }
            bk.delete();
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });

});

router.post('/restore/:id', authQlht, (req, res) => {
    Backup.findById(req.params.id).then((bk) => {
        if (!bk) res.status(404).json({ Error: "Không tìm thấy bản backup này!" });
        else {
            pa = path.join(__dirname + '/../backup', bk.name + '/\hrm2019');
            let cmd =
                'mongorestore --host ' +
                dbOptions.host +
                ' --port ' +
                dbOptions.port +
                ' --db ' +
                dbOptions.database +
                ' --username ' +
                dbOptions.user +
                ' --password ' +
                dbOptions.pass +
                ' --drop -d ' +
                dbOptions.database +
                ' ' + pa;
            if (fs.existsSync(pa)) {
                console.log(cmd);
                exec(cmd);
                Acc.find({}).then(accs => {
                    for(let acc of accs){
                      acc.isLogin = 0;
                      acc.save();
                    }
                });
                res.status(200).json({ Message: "Phục hồi dữ liệu thành công!" });
            }
            else res.status(400).json({ Message: "Phục hồi dữ liệu thất bại!" });
            //res.status(404).json({ Mess: "Phục hồi dữ liệu thành công!" });
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

module.exports = router;