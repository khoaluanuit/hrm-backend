const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const isEmpty = require("is-empty");

const Tkp = require("../models/timekeeping");
const Emp = require("../models/employee");

const auth = require("../middleware/auth");
const authQlnp = require("../middleware/auth-qlnp");

const ValidateTkpInput = require("../validation/timekeepings");

router.post('/create', auth, (req, res) => {
    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) return res.status(404).json(errors);

    let month = req.body.month;
    let year = req.body.year;
    daysIn = req.body.days;
    // cần truyền đối tượng days như sau:
    // let daysIn = [
    //     {
    //         "date": req.body.date1,
    //         "reason": req.body.reason1,
    //         "empBackup": req.body.empBackup1,
    //         "contact": req.body.contact1
    //     }];
    //     {
    //         "date": req.body.date2,
    //         "reason": req.body.reason2,
    //     }
    // ];

    let now = new Date();
    if (year < now.getUTCFullYear()) return res.status(404).json({ message: "Đã hết thời hạn để có thể tạo đơn nghỉ phép, đơn nghỉ phép chỉ có thể được tạo trong 72h sau khi bạn nghỉ!" });
    else if (year == now.getFullYear() && month < now.getMonth() + 1) {
        return res.status(404).json({ message: "Đã hết thời hạn để có thể tạo đơn nghỉ phép, đơn nghỉ phép chỉ có thể được tạo trong 72h sau khi bạn nghỉ!" });
    }

    const enToken = req.header('x-auth-token');
    const token = enToken.slice(0,30) + enToken.slice(30,50).split("").reverse().join("") + enToken.slice(50);
    const decoded = jwt.verify(token, process.env.JWT);

    Emp.findOne({ account: decoded.email }).then(emp => {
        if (!emp) return res.status(404).json({ message: "Vui lòng báo quản trị viên cập nhật thông tin hồ sơ của bạn, sau đó thử lại!" });
        Tkp.findOne({ idEmp: emp.idEmp, month, year }).then(tkp => {
            if (!tkp) return res.status(404).json({ message: "Bảng chấm công cho tháng này chưa được tạo!" });
            else {
                let days = [];
                // test dataIn
                // let daysIn = [
                //     {
                //         "date": req.body.date1,
                //         "reason": req.body.reason1,
                //         "empBackup": req.body.empBackup1,
                //         "contact": req.body.contact1
                //     }];
                //         // {
                //         //     "date": req.body.date2,
                //         //     "reason": req.body.reason2,
                //         // }
                // ];
                for (let i = 0; i < tkp.days.length; i++) {
                    if (tkp.days[i].leave.contact) {
                        days.push({
                            "date": tkp.days[i].date,
                            "daysOfW": tkp.days[i].daysOfW,
                            "checkIn": tkp.days[i].checkIn,
                            "checkOut": tkp.days[i].checkOut,
                            "lateEarly": tkp.days[i].lateEarly,
                            "leave": {
                                "permit": tkp.days[i].leave.permit,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "note": tkp.days[i].leave.note,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                            }
                        });
                    } else {
                        days.push({
                            "date": tkp.days[i].date,
                            "daysOfW": tkp.days[i].daysOfW,
                            "checkIn": tkp.days[i].checkIn,
                            "checkOut": tkp.days[i].checkOut,
                            "lateEarly": tkp.days[i].lateEarly,
                            "leave": {
                                "permit": tkp.days[i].leave.permit,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "note": tkp.days[i].leave.note,
                                "empBackup": "None",
                                "contact": "None",
                                "dateOfProcessing": "None",
                                "dateCreate": "None"
                            }
                        });
                    }
                }
                for (let i = 0; i < daysIn.length; i++) {
                    if (days[daysIn[i].date - 1].daysOfW == 6 || days[daysIn[i].date - 1].daysOfW == 7){
                        return res.status(404).json({ message: "Thứ 7, chủ nhật không cần tạo phép!" });
                    }
                    else if (days[daysIn[i].date - 1].leave.reason != "None") {
                        return res.status(404).json({ message: "Bạn đã tạo đơn nghỉ phép cho ngày này rồi!" });
                    }
                    else if (daysIn[i].reason === "None" || isEmpty(daysIn[i].reason) === true) {
                        return res.status(404).json({ message: "Vui lòng nhập lý do xin nghỉ phép!" });
                    }
                    else {
                        if (now.getDate() - days[daysIn[i].date - 1].date > 3) {
                            return res.status(404).json({ message: "Đã hết thời hạn để có thể tạo đơn nghỉ phép, đơn nghỉ phép chỉ có thể được tạo trong 72h sau khi bạn nghỉ!" });
                        } else {
                            let now = new Date();
                            days[daysIn[i].date - 1].leave.reason = daysIn[i].reason;
                            days[daysIn[i].date - 1].leave.note = "Nghỉ không phép (Chưa duyệt)";
                            days[daysIn[i].date - 1].leave.empBackup = daysIn[i].empBackup;
                            days[daysIn[i].date - 1].leave.contact = daysIn[i].contact;
                            days[daysIn[i].date - 1].leave.dateCreate = now;
                        }
                    }
                }

                tkp.days = days;
                tkp.save().then(tkp => res.send(tkp));
            }

        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/delete', auth, (req, res) => {
    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) return res.status(404).json(errors);

    let month = req.body.month;
    let year = req.body.year;
    let date = req.body.date;

    const enToken = req.header('x-auth-token');
    const token = enToken.slice(0,30) + enToken.slice(30,50).split("").reverse().join("") + enToken.slice(50);
    const decoded = jwt.verify(token, process.env.JWT);

    Emp.findOne({ account: decoded.email }).then(emp => {
        if (!emp) return res.status(404).json({ message: "Vui lòng báo quản trị viên cập nhật thông tin hồ sơ của bạn, sau đó thử lại!" });
        Tkp.findOne({ month, year, idEmp: emp.idEmp }).then(tkp => {
            let days = [];
            let now = new Date();
            let success = 0;
            for (let i = 0; i < tkp.days.length; i++) {
                days.push({
                    "date": tkp.days[i].date,
                    "daysOfW": tkp.days[i].daysOfW,
                    "checkIn": tkp.days[i].checkIn,
                    "checkOut": tkp.days[i].checkOut,
                    "lateEarly": tkp.days[i].lateEarly,
                    "leave": {
                        "permit": tkp.days[i].leave.permit,
                        "reason": tkp.days[i].leave.reason,
                        "approver": tkp.days[i].leave.approver,
                        "note": tkp.days[i].leave.note,
                        "dateCreate": tkp.days[i].leave.dateCreate,
                        "empBackup": tkp.days[i].leave.empBackup,
                        "contact": tkp.days[i].leave.contact,
                        "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                    }
                });
                if (days[i].date == date && date < now.getDate() && days[i].leave.approver != "None") {
                    return res.status(400).json({ warning: "Đơn này đã được duyệt, không được phép xóa!" });
                }
                if (days[i].date == date && days[i].leave.reason === "None") return res.status(400).json({ warning: "Không tồn tại đơn nghỉ phép này!" });
                if (days[i].date == date && date >= now.getDate()) {
                    days[i].leave.reason = "None";
                    days[i].leave.approver = "None";
                    days[i].leave.permit = false;
                    days[i].leave.note = "None";
                    days[i].leave.dateOfProcessing = "None";
                    days[i].leave.dateCreate = "None";
                    days[i].leave.empBackup = "None";
                    days[i].leave.contact = "None"
                    success = 1;
                } else if (days[i].date == date) {
                    days[i].leave.reason = "None";
                    days[i].leave.approver = "None";
                    days[i].leave.permit = false;
                    days[i].leave.note = "Nghỉ không phép.";
                    days[i].leave.dateOfProcessing = "None";
                    days[i].leave.dateCreate = "None";
                    days[i].leave.empBackup = "None";
                    days[i].leave.contact = "None";
                    success = 1;
                }
            }
            if (success === 0) return res.status(404).json({ warning: "Xóa thất bại!" });
            else {
                tkp.days = days;
                tkp.save();
                res.status(200).json({ message: "Xóa thành công!" });
            }
        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
// get your leaves in month
router.post('/month/yourleaves', auth, (req, res) => {
    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) res.status(404).json({ errors });
    let month = req.body.month;
    let year = req.body.year;
    const enToken = req.header('x-auth-token');
    const token = enToken.slice(0,30) + enToken.slice(30,50).split("").reverse().join("") + enToken.slice(50);
    let decoded = jwt.verify(token, process.env.JWT);

    Emp.findOne({ account: decoded.email }).then(emp => {
        Tkp.findOne({ idEmp: emp.idEmp, month, year }).then(tkp => {
            if (!tkp) return res.status(404).json({ message: "Bảng chấm công của tháng này chưa được tạo" });
            else {
                let leaves = [];
                let leavesAll = [];
                let leavesNotApproveYet = [];
                let leavesNoPermit = [];
                let leavesPermit = [];
                for (let i = 0; i < tkp.days.length; i++) {
                    if (tkp.days[i].leave.reason != "None") {
                        leavesAll.push({
                            "idEmp": tkp.idEmp,
                            "date": i + 1,
                            "daysOfW": tkp.days[i].daysOfW,
                            "reason": tkp.days[i].leave.reason,
                            "approver": tkp.days[i].leave.approver,
                            "permit": tkp.days[i].leave.permit,
                            "dateCreate": tkp.days[i].leave.dateCreate,
                            "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                            "contact": tkp.days[i].leave.contact,
                            "empBackup": tkp.days[i].leave.empBackup,
                            "note": tkp.days[i].leave.note
                        });
                    }
                    if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver === "None") {
                        leavesNotApproveYet.push({
                            "idEmp": tkp.idEmp,
                            "date": i + 1,
                            "daysOfW": tkp.days[i].daysOfW,
                            "reason": tkp.days[i].leave.reason,
                            "approver": tkp.days[i].leave.approver,
                            "permit": tkp.days[i].leave.permit,
                            "dateCreate": tkp.days[i].leave.dateCreate,
                            "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                            "empBackup": tkp.days[i].leave.empBackup,
                            "contact": tkp.days[i].leave.contact,
                            "note": tkp.days[i].leave.note
                        });
                    }
                    if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                        && tkp.days[i].leave.permit === false) {
                        leavesNoPermit.push({
                            "idEmp": tkp.idEmp,
                            "date": i + 1,
                            "daysOfW": tkp.days[i].daysOfW,
                            "reason": tkp.days[i].leave.reason,
                            "approver": tkp.days[i].leave.approver,
                            "permit": tkp.days[i].leave.permit,
                            "dateCreate": tkp.days[i].leave.dateCreate,
                            "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                            "empBackup": tkp.days[i].leave.empBackup,
                            "contact": tkp.days[i].leave.contact,
                            "note": tkp.days[i].leave.note
                        });
                    }
                    if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                        && tkp.days[i].leave.permit === true) {
                        leavesPermit.push({
                            "idEmp": tkp.idEmp,
                            "date": i + 1,
                            "daysOfW": tkp.days[i].daysOfW,
                            "reason": tkp.days[i].leave.reason,
                            "approver": tkp.days[i].leave.approver,
                            "permit": tkp.days[i].leave.permit,
                            "dateCreate": tkp.days[i].leave.dateCreate,
                            "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                            "empBackup": tkp.days[i].leave.empBackup,
                            "contact": tkp.days[i].leave.contact,
                            "note": tkp.days[i].leave.note
                        });
                    }
                }
                leaves = { leavesAll, leavesNotApproveYet, leavesPermit, leavesNoPermit };
                res.send(leaves);
            }
        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/year/yourleaves', auth, (req, res) => {
    year = req.body.year;
    const enToken = req.header('x-auth-token');
    const token = enToken.slice(0,30) + enToken.slice(30,50).split("").reverse().join("") + enToken.slice(50);
    decoded = jwt.verify(token, process.env.JWT);

    if (isEmpty(year)) return res.status(404).json({ error: "Vui lòng chọn năm!" });
    if (year > 10000 && year < 2018) return res.status(404).json({ error: "Vui lòng chọn lại năm!" });

    Emp.findOne({ account: decoded.email }).then(emp => {
        Tkp.find({ year }).then(tkps => {
            if (tkps.length < 1) return res.status(400).json({ message: "Chưa có bảng chấm công nào cho năm này được tạo!" });
            let leaves = [];
            let leavesAll = [];
            let leavesNotApproveYet = [];
            let leavesNoPermit = [];
            let leavesPermit = [];
            for (let tkp of tkps) {
                if (emp.idEmp === tkp.idEmp) {
                    for (let i = 0; i < tkp.days.length; i++) {
                        if (tkp.days[i].leave.reason != "None") {
                            leavesAll.push({
                                "idEmp": tkp.idEmp,
                                "date": i + 1,
                                "month": tkp.month,
                                "daysOfW": tkp.days[i].daysOfW,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "permit": tkp.days[i].leave.permit,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "note": tkp.days[i].leave.note
                            });
                        }
                        if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver === "None") {
                            leavesNotApproveYet.push({
                                "idEmp": tkp.idEmp,
                                "date": i + 1,
                                "month": tkp.month,
                                "daysOfW": tkp.days[i].daysOfW,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "permit": tkp.days[i].leave.permit,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "note": tkp.days[i].leave.note
                            });
                        }
                        if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                            && tkp.days[i].leave.permit === false) {
                            leavesNoPermit.push({
                                "idEmp": tkp.idEmp,
                                "date": i + 1,
                                "month": tkp.month,
                                "daysOfW": tkp.days[i].daysOfW,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "permit": tkp.days[i].leave.permit,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "note": tkp.days[i].leave.note
                            });
                        }
                        if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                            && tkp.days[i].leave.permit === true) {
                            leavesPermit.push({
                                "idEmp": tkp.idEmp,
                                "date": i + 1,
                                "month": tkp.month,
                                "daysOfW": tkp.days[i].daysOfW,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "permit": tkp.days[i].leave.permit,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "note": tkp.days[i].leave.note
                            });
                        }
                    }
                }
            }
            console.log(leavesAll.length);
            if (leavesAll.length < 1) return res.status(400).json({ message: "Không có đơn nghỉ phép nào!" });
            leaves = { leavesAll, leavesNotApproveYet, leavesPermit, leavesNoPermit };
            res.send(leaves);
        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

// get all leaves for user login
router.get('/yourleaves', auth, (req, res) => {
    const enToken = req.header('x-auth-token');
    const token = enToken.slice(0,30) + enToken.slice(30,50).split("").reverse().join("") + enToken.slice(50);
    decoded = jwt.verify(token, process.env.JWT);
    Emp.findOne({ account: decoded.email }).then(emp => {
        Tkp.find({}).then(tkps => {
            if (tkps.length < 1) return res.status(400).json({ message: "Chưa có bảng chấm công nào được tạo!" });
            let leaves = [];
            let leavesAll = [];
            let leavesNotApproveYet = [];
            let leavesNoPermit = [];
            let leavesPermit = [];
            for (let tkp of tkps) {
                if (emp.idEmp === tkp.idEmp) {
                    for (let i = 0; i < tkp.days.length; i++) {
                        if (tkp.days[i].daysOfW != 6 && tkp.days[i].daysOfW != 7){
                            if (tkp.days[i].leave.reason != "None") {
                                leavesAll.push({
                                    "idEmp": tkp.idEmp,
                                    "date": i + 1,
                                    "month": tkp.month,
                                    "year": tkp.year,
                                    "daysOfW": tkp.days[i].daysOfW,
                                    "reason": tkp.days[i].leave.reason,
                                    "approver": tkp.days[i].leave.approver,
                                    "permit": tkp.days[i].leave.permit,
                                    "dateCreate": tkp.days[i].leave.dateCreate,
                                    "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                    "empBackup": tkp.days[i].leave.empBackup,
                                    "contact": tkp.days[i].leave.contact,
                                    "note": tkp.days[i].leave.note
                                });
                            }
                            if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver === "None") {
                                leavesNotApproveYet.push({
                                    "idEmp": tkp.idEmp,
                                    "date": i + 1,
                                    "month": tkp.month,
                                    "year": tkp.year,
                                    "daysOfW": tkp.days[i].daysOfW,
                                    "reason": tkp.days[i].leave.reason,
                                    "approver": tkp.days[i].leave.approver,
                                    "permit": tkp.days[i].leave.permit,
                                    "dateCreate": tkp.days[i].leave.dateCreate,
                                    "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                    "empBackup": tkp.days[i].leave.empBackup,
                                    "contact": tkp.days[i].leave.contact,
                                    "note": tkp.days[i].leave.note
                                });
                            }
                            if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                                && tkp.days[i].leave.permit === false) {
                                leavesNoPermit.push({
                                    "idEmp": tkp.idEmp,
                                    "date": i + 1,
                                    "month": tkp.month,
                                    "year": tkp.year,
                                    "daysOfW": tkp.days[i].daysOfW,
                                    "reason": tkp.days[i].leave.reason,
                                    "approver": tkp.days[i].leave.approver,
                                    "permit": tkp.days[i].leave.permit,
                                    "dateCreate": tkp.days[i].leave.dateCreate,
                                    "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                    "empBackup": tkp.days[i].leave.empBackup,
                                    "contact": tkp.days[i].leave.contact,
                                    "note": tkp.days[i].leave.note
                                });
                            }
                            if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                                && tkp.days[i].leave.permit === true) {
                                leavesPermit.push({
                                    "idEmp": tkp.idEmp,
                                    "date": i + 1,
                                    "month": tkp.month,
                                    "year": tkp.year,
                                    "daysOfW": tkp.days[i].daysOfW,
                                    "reason": tkp.days[i].leave.reason,
                                    "approver": tkp.days[i].leave.approver,
                                    "permit": tkp.days[i].leave.permit,
                                    "dateCreate": tkp.days[i].leave.dateCreate,
                                    "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                    "empBackup": tkp.days[i].leave.empBackup,
                                    "contact": tkp.days[i].leave.contact,
                                    "note": tkp.days[i].leave.note
                                });
                            }
                        }
                    }
                }
            }
            if (leavesAll.length < 1) return res.status(400).json({ Message: "Bạn chưa tạo đơn nghỉ phép nào!" });
            leaves = { leavesAll, leavesNotApproveYet, leavesPermit, leavesNoPermit };
            res.send(leaves);
        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});


// get all leaves in a month
router.post('/month', authQlnp, (req, res) => {
    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) return res.status(404).json({ errors });
    month = req.body.month;
    year = req.body.year;
    Tkp.find({ month, year }).then(tkps => {
        if (tkps.length < 1) return res.status(404).json({ message: "Bảng chấm công cho tháng này chưa được tạo!" });
        else {
            let leaves = [];
            let leavesAll = [];
            let leavesNotApproveYet = [];
            let leavesNoPermit = [];
            let leavesPermit = [];
            Emp.find({}).then(emps => {
                for (let tkp of tkps) {
                    var emp = emps.find(function (empl) {
                        return empl.idEmp === tkp.idEmp;
                    });
                    if (!emp) return res.status(404).json({ message: "Hồ sơ nhân viên có mã " + tkp.idEmp + "!" });
                    for (let i = 0; i < tkp.days.length; i++) {
                        if (tkp.days[i].leave.reason != "None") {
                            leavesAll.push({
                                "idEmp": tkp.idEmp,
                                "nameEmp": emp.fullname,
                                "date": i + 1,
                                "daysOfW": tkp.days[i].daysOfW,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "permit": tkp.days[i].leave.permit,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "note": tkp.days[i].leave.note
                            });
                        }
                        if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver === "None") {
                            leavesNotApproveYet.push({
                                "idEmp": tkp.idEmp,
                                "nameEmp": emp.fullname,
                                "date": i + 1,
                                "daysOfW": tkp.days[i].daysOfW,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "permit": tkp.days[i].leave.permit,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "note": tkp.days[i].leave.note
                            });
                        }
                        if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                            && tkp.days[i].leave.permit === false) {
                            leavesNoPermit.push({
                                "idEmp": tkp.idEmp,
                                "nameEmp": emp.fullname,
                                "date": i + 1,
                                "daysOfW": tkp.days[i].daysOfW,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "contact": tkp.days[i].leave.contact,
                                "permit": tkp.days[i].leave.permit,
                                "note": tkp.days[i].leave.note
                            });
                        }
                        if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                            && tkp.days[i].leave.permit === true) {
                            leavesPermit.push({
                                "idEmp": tkp.idEmp,
                                "nameEmp": emp.fullname,
                                "date": i + 1,
                                "daysOfW": tkp.days[i].daysOfW,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "permit": tkp.days[i].leave.permit,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "note": tkp.days[i].leave.note
                            });
                        }
                    }
                }
                leaves = { leavesAll, leavesNotApproveYet, leavesPermit, leavesNoPermit };
                res.send(leaves);
            });
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/year', authQlnp, (req, res) => {
    year = req.body.year;
    if (isEmpty(year)) return res.status(404).json({ error: "Vui lòng chọn năm!" });
    if (year > 10000 && year < 2018) return res.status(404).json({ error: "Vui lòng chọn lại năm!" });

    Tkp.find({ year }).then(tkps => {
        if (tkps.length < 1) return res.status(404).json({ message: "Bảng chấm công cho năm này chưa được tạo!" });
        else {
            let leaves = [];
            let leavesAll = [];
            let leavesNotApproveYet = [];
            let leavesNoPermit = [];
            let leavesPermit = [];
            Emp.find({}).then(emps => {
                for (let tkp of tkps) {
                    var emp = emps.find(function (empl) {
                        return empl.idEmp === tkp.idEmp;
                    });
                    if (!emp) return res.status(404).json({ message: "Hồ sơ nhân viên có mã " + tkp.idEmp + "!" });
                    for (let i = 0; i < tkp.days.length; i++) {
                        if (tkp.days[i].leave.reason != "None") {
                            leavesAll.push({
                                "idEmp": tkp.idEmp,
                                "nameEmp": emp.fullname,
                                "date": i + 1,
                                "month": tkp.month,
                                "daysOfW": tkp.days[i].daysOfW,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "permit": tkp.days[i].leave.permit,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "note": tkp.days[i].leave.note
                            });
                        }
                        if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver === "None") {
                            leavesNotApproveYet.push({
                                "idEmp": tkp.idEmp,
                                "nameEmp": emp.fullname,
                                "date": i + 1,
                                "daysOfW": tkp.days[i].daysOfW,
                                "month": tkp.month,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "permit": tkp.days[i].leave.permit,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "note": tkp.days[i].leave.note
                            });
                        }
                        if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                            && tkp.days[i].leave.permit === false) {
                            leavesNoPermit.push({
                                "idEmp": tkp.idEmp,
                                "nameEmp": emp.fullname,
                                "date": i + 1,
                                "daysOfW": tkp.days[i].daysOfW,
                                "month": tkp.month,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "permit": tkp.days[i].leave.permit,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "note": tkp.days[i].leave.note
                            });
                        }
                        if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                            && tkp.days[i].leave.permit === true) {
                            leavesPermit.push({
                                "idEmp": tkp.idEmp,
                                "nameEmp": emp.fullname,
                                "date": i + 1,
                                "daysOfW": tkp.days[i].daysOfW,
                                "month": tkp.month,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "permit": tkp.days[i].leave.permit,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "note": tkp.days[i].leave.note
                            });
                        }
                    }

                }
                leaves = { leavesAll, leavesNotApproveYet, leavesPermit, leavesNoPermit };
                res.send(leaves);
            });
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.get('/', authQlnp, (req, res) => {
    Tkp.find({}).then(tkps => {
        if (tkps.length < 1) return res.status(404).json({ message: "Bảng chấm công cho tháng này chưa được tạo!" });
        else {
            let leaves = [];
            let leavesAll = [];
            let leavesNotApproveYet = [];
            let leavesNoPermit = [];
            let leavesPermit = [];
            Emp.find({}).then(emps => {
                for (let tkp of tkps) {
                    var emp = emps.find(function (empl) {
                        return empl.idEmp === tkp.idEmp;
                    });
                    for (let i = 0; i < tkp.days.length; i++) {
                        if (tkp.days[i].daysOfW != 6 && tkp.days[i].daysOfW != 7){
                            if (tkp.days[i].leave.reason != "None" && emp) {
                                leavesAll.push({
                                    "idEmp": tkp.idEmp,
                                    "nameEmp": emp.fullname,
                                    "date": i + 1,
                                    "month": tkp.month,
                                    "year": tkp.year,
                                    "daysOfW": tkp.days[i].daysOfW,
                                    "reason": tkp.days[i].leave.reason,
                                    "approver": tkp.days[i].leave.approver,
                                    "permit": tkp.days[i].leave.permit,
                                    "dateCreate": tkp.days[i].leave.dateCreate,
                                    "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                    "empBackup": tkp.days[i].leave.empBackup,
                                    "contact": tkp.days[i].leave.contact,
                                    "note": tkp.days[i].leave.note
                                });
                            }
                            if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver === "None" && emp) {
                                leavesNotApproveYet.push({
                                    "idEmp": tkp.idEmp,
                                    "nameEmp": emp.fullname,
                                    "date": i + 1,
                                    "daysOfW": tkp.days[i].daysOfW,
                                    "month": tkp.month,
                                    "year": tkp.year,
                                    "reason": tkp.days[i].leave.reason,
                                    "approver": tkp.days[i].leave.approver,
                                    "permit": tkp.days[i].leave.permit,
                                    "dateCreate": tkp.days[i].leave.dateCreate,
                                    "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                    "empBackup": tkp.days[i].leave.empBackup,
                                    "contact": tkp.days[i].leave.contact,
                                    "note": tkp.days[i].leave.note
                                });
                            }
                            if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                                && tkp.days[i].leave.permit === false && emp) {
                                leavesNoPermit.push({
                                    "idEmp": tkp.idEmp,
                                    "nameEmp": emp.fullname,
                                    "date": i + 1,
                                    "daysOfW": tkp.days[i].daysOfW,
                                    "month": tkp.month,
                                    "year": tkp.year,
                                    "reason": tkp.days[i].leave.reason,
                                    "approver": tkp.days[i].leave.approver,
                                    "permit": tkp.days[i].leave.permit,
                                    "dateCreate": tkp.days[i].leave.dateCreate,
                                    "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                    "empBackup": tkp.days[i].leave.empBackup,
                                    "contact": tkp.days[i].leave.contact,
                                    "note": tkp.days[i].leave.note
                                });
                            }
                            if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                                && tkp.days[i].leave.permit === true && emp) {
                                leavesPermit.push({
                                    "idEmp": tkp.idEmp,
                                    "nameEmp": emp.fullname,
                                    "date": i + 1,
                                    "daysOfW": tkp.days[i].daysOfW,
                                    "month": tkp.month,
                                    "year": tkp.year,
                                    "reason": tkp.days[i].leave.reason,
                                    "approver": tkp.days[i].leave.approver,
                                    "permit": tkp.days[i].leave.permit,
                                    "dateCreate": tkp.days[i].leave.dateCreate,
                                    "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                    "empBackup": tkp.days[i].leave.empBackup,
                                    "contact": tkp.days[i].leave.contact,
                                    "note": tkp.days[i].leave.note
                                });
                            }
                        }
                    }

                }
                leaves = { leavesAll, leavesNotApproveYet, leavesPermit, leavesNoPermit };
                res.send(leaves);
            });
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

// get day leaves haven't approved yet
// router.post('/month/notapprovedyet', (req, res) => {
//     const { errors, isValid } = ValidateTkpInput(req.body);
//     if (!isValid) return res.status(404).json({ errors });
//     month = req.body.month;
//     year = req.body.year;

//     Tkp.find({ month, year }).then(tkps => {
//         if (tkps.length < 1) res.status(404).json({ message: "Bảng chấm công cho tháng này chưa được tạo!" });
//         else {
//             let leaves = [];
//             for (let tkp of tkps) {
//                 for (let i = 0; i < tkp.days.length; i++) {
//                     if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver === "None") {
//                         leaves.push({
//                             "idEmp": tkp.idEmp,
//                             "date": i + 1,
//                             "daysOfW": tkp.days[i].daysOfW,
//                             "month": tkp.month,
//                             "year": tkp.year,
//                             "reason": tkp.days[i].leave.reason,
//                             "approver": tkp.days[i].leave.approver,
//                             "permit": tkp.days[i].leave.permit
//                         });
//                     }
//                 }
//             }
//             res.send(leaves);
//         }
//     })
//         .catch(err => {
//             console.log(err);
//             res.status(500).json({
//                 error: err
//             });
//         });
// });

// get day leaves no permit
// router.post('/month/nopermit', (req, res) => {
//     const { errors, isValid } = ValidateTkpInput(req.body);
//     if (!isValid) return res.status(404).json({ errors });
//     month = req.body.month;
//     year = req.body.year;

//     Tkp.find({ month, year }).then(tkps => {
//         if (tkps.length < 1) res.status(404).json({ message: "Bảng chấm công cho tháng này chưa được tạo!" });
//         else {
//             let leaves = [];
//             for (let tkp of tkps) {
//                 for (let i = 0; i < tkp.days.length; i++) {
//                     if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
//                     && tkp.days[i].leave.permit === false) {
//                         leaves.push({
//                             "idEmp": tkp.idEmp,
//                             "date": i + 1,
//                             "daysOfW": tkp.days[i].daysOfW,
//                             "month": tkp.month,
//                             "year": tkp.year,
//                             "reason": tkp.days[i].leave.reason,
//                             "approver": tkp.days[i].leave.approver,
//                             "permit": tkp.days[i].leave.permit
//                         });
//                     }
//                 }
//             }
//             res.send(leaves);
//         }
//     })
//         .catch(err => {
//             console.log(err);
//             res.status(500).json({
//                 error: err
//             });
//         });
// });

// get all day leaves in month
router.post('/month123', authQlnp, (req, res) => {
    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) return res.status(404).json({ errors });
    month = req.body.month;
    year = req.body.year;

    Tkp.find({ month, year }).then(tkps => {
        if (tkps.length < 1) res.status(404).json({ message: "Bảng chấm công cho tháng này chưa được tạo!" });
        else {
            tkp = tkps[0];
            let leavesDate = [];
            let leaves = 0; // tổng đơn phép
            let noApprove = 0; // tổng đơn phép chưa được duyệt
            let noPermit = 0; // tổng đơn phép không được duyệt
            let permit = 0; // tổng đơn phép đã duyệt
            for (let i = 0; i < tkp.days.length; i++) {
                for (let tkp of tkps) {
                    if (tkp.days[i].leave.reason != "None") leaves++;
                    if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver === "None") noApprove++;
                    if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                        && tkp.days[i].leave.permit === false) noPermit++;
                    if (tkp.days[i].leave.reason != "None" && tkp.days[i].leave.approver != "None"
                        && tkp.days[i].leave.permit === true) permit++;
                }
            }
            leavesDate.push({ leaves, noApprove, permit, noPermit });
            res.send(leavesDate);
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/approve', authQlnp, (req, res) => {
    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) return res.status(404).json({ errors });
    month = req.body.month;
    year = req.body.year;
    leaves = req.body.leaves;

    // leaves = [
    //     {
    //         "action": req.body.action1, // nhận giá trị là approve hoặc delete, 
    //                                        approve để tiến hành duyệt, delete để xóa duệt
    //         "date": req.body.date1,     // ngày 
    //         "idEmp": req.body.idEmp1,   // id nhân viên xin nghỉ
    //         "permit": req.body.permit1, // permit nhận true hoặc false
    //         "note": req.body.note1,     // ghi chú của người chấm công
    //     }
    // {
    //     "action": req.body.action2,
    //     "date": req.body.date2,
    //     "idEmp": req.body.idEmp2,
    //     "permit": req.body.permit2,
    //     "note": req.body.note2,
    // }
    //];

    const enToken = req.header('x-auth-token');
    const token = enToken.slice(0,30) + enToken.slice(30,50).split("").reverse().join("") + enToken.slice(50);
    decoded = jwt.verify(token, process.env.JWT);

    Tkp.find({ month, year }).then(tkps => {
        if (tkps.length < 1) return res.status(404).json({ message: "Bảng chấm công cho tháng này chưa được tạo!" });
        else {
            let actions = 0;
            // leaves = [
            //     {
            //         "date": req.body.date1,
            //         "idEmp": req.body.idEmp1,
            //         "permit": req.body.permit1,
            //         "note": req.body.note1,
            //     }];
            // {
            //     "action": req.body.action2,
            //     "date": req.body.date2,
            //     "idEmp": req.body.idEmp2,
            //     "permit": req.body.permit2,
            //     "note": req.body.note2,
            // }
            //];
            Emp.findOne({ account: decoded.email }).then(emp => {
                for (let tkp of tkps) {
                    for (let j = 0; j < leaves.length; j++) {
                        if (!emp) return res.status(400).json({ warning: "Hồ sơ cá nhân của bạn chưa được cập nhật, nên bạn không được phép thực hiện hành động này." });
                        else if (leaves[j].idEmp === emp.idEmp) {
                            return res.status(404).json({ message: "Bạn không được có quyền duyệt phép của bản thân mình!" });
                        }
                        if (tkp.idEmp === leaves[j].idEmp) {
                            let days = [];
                            for (let i = 0; i < tkp.days.length; i++) {
                                if (tkp.days[i].leave.dateCreate) {
                                    days.push({
                                        "date": tkp.days[i].date,
                                        "daysOfW": tkp.days[i].daysOfW,
                                        "checkIn": tkp.days[i].checkIn,
                                        "checkOut": tkp.days[i].checkOut,
                                        "lateEarly": tkp.days[i].lateEarly,
                                        "leave": {
                                            "permit": tkp.days[i].leave.permit,
                                            "reason": tkp.days[i].leave.reason,
                                            "approver": tkp.days[i].leave.approver,
                                            "note": tkp.days[i].leave.note,
                                            "empBackup": tkp.days[i].leave.empBackup,
                                            "contact": tkp.days[i].leave.contact,
                                            "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                                            "dateCreate": tkp.days[i].leave.dateCreate
                                        }
                                    });
                                } else {
                                    days.push({
                                        "date": tkp.days[i].date,
                                        "daysOfW": tkp.days[i].daysOfW,
                                        "checkIn": tkp.days[i].checkIn,
                                        "checkOut": tkp.days[i].checkOut,
                                        "lateEarly": tkp.days[i].lateEarly,
                                        "leave": {
                                            "permit": tkp.days[i].leave.permit,
                                            "reason": tkp.days[i].leave.reason,
                                            "approver": tkp.days[i].leave.approver,
                                            "note": tkp.days[i].leave.note,
                                            "empBackup": "None",
                                            "contact": "None",
                                            "dateOfProcessing": "None",
                                            "dateCreate": "None"
                                        }
                                    });
                                }
                            }
                            var now = new Date();
                            now.setHours(now.getHours() + 7);
                            if (leaves[j].permit === false && isEmpty(leaves[j].note)) {
                                return res.status(404).json({ message: "Vui lòng điền lý do đơn nghỉ phép này không được duyệt!" });
                            }
                            else if (leaves[j].permit === false) {
                                days[leaves[j].date - 1].leave.permit = false;
                                days[leaves[j].date - 1].leave.note = "Nghỉ có phép nhưng không được duyệt (" + leaves[j].note + ").";
                                Emp.findOne({ account: decoded.email }).then(emp => {
                                    days[leaves[j].date - 1].leave.approver = emp.fullname + " (" + emp.idEmp + ")";
                                    days[leaves[j].date - 1].leave.dateOfProcessing = now;
                                    tkp.days = days;
                                    tkp.save();
                                });
                                actions = 1;
                            }
                            else if (leaves[j].permit === true) {
                                days[leaves[j].date - 1].leave.permit = true;
                                days[leaves[j].date - 1].leave.note = "Nghỉ có phép.";
                                Emp.findOne({ account: decoded.email }).then(emp => {
                                    days[leaves[j].date - 1].leave.approver = emp.fullname + " (" + emp.idEmp + ")";
                                    days[leaves[j].date - 1].leave.dateOfProcessing = now;
                                    tkp.days = days;
                                    tkp.save();
                                });
                                actions = 1;
                            }

                        }
                    }
                    let leavesPermit = 0;
                    for (let i = 0; i < tkp.days.length; i++) {
                        if (tkp.days[i].leave.permit === true) leavesPermit++;
                    }
                    tkp.leavePermit = leavesPermit;
                    tkp.save();
                }
                if (actions === 0) return res.status(200).json({ message: "Duyệt phép không thành công!" });
                else return res.status(200).json({ message: "Duyệt phép thành công!" });
            });
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

module.exports = router;