const express = require("express");
const router = express.Router();
const isEmpty = require("is-empty");

// Get GeneralInfo model
const Gen = require("../models/generalInfo");

const auth = require("../middleware/auth");
const authQlns = require("../middleware/auth-qlns");

// Get validate module
const validateGenInput = require("../validation/generalInfo");

// Only use for developer
router.post('/create', auth, (req, res) => {
    const newGen = new Gen({
            "socialInsuC" : 0.175,
            "socialInsuE" : 0.08,
            "healthInsuC" : 0.033,
            "healthInsuE" : 0.015,
            "unempInsuC" : 0.01,
            "unempInsuE" : 0.011,
            "payDate" : "Mùng năm đầu mỗi tháng.",
            "payForms" : "Trả qua thẻ ngân hàng.",
            "lateEarly": 1,
            "ovtHour": 1.2,
            "ovtHourInDateOff": 1.8,
            "ovtHourInPubHoli": 3.6,
            "workInDateOff": 1.5,
            "workInPubHoli": 3,
            "nightShift": 1.5,
            "nightShiftInDateOff": 2.25,
            "nightShiftInPubHoli": 4.5,
            "dateModified" : ""
    });
    newGen.save().then(gen => console.log(gen))
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
    res.send("Successfully");
});

router.post('/edit', authQlns, (req, res) => {
    const {errors, isValid} = validateGenInput(req.body);
    if (!isValid) return res.status(404).json(errors);
    
    now = new Date();
    Gen.find({}).then(gens => {
        let gen = gens[0];
        gen.socialInsuC =  isEmpty(req.body.socialInsuC) ? gen.socialInsuC : req.body.socialInsuC,
        gen.socialInsuE = isEmpty(req.body.socialInsuE) ? gen.socialInsuE : req.body.socialInsuE,
        gen.healthInsuC = isEmpty(req.body.healthInsuC) ? gen.healthInsuC : req.body.healthInsuC,
        gen.healthInsuE = isEmpty(req.body.healthInsuE) ? gen.healthInsuE : req.body.healthInsuE,
        gen.unempInsuC = isEmpty(req.body.unempInsuC) ? gen.unempInsuC : req.body.unempInsuC,
        gen.unempInsuE = isEmpty(req.body.unempInsuE) ? gen.unempInsuE : req.body.unempInsuE,
        gen.payDate = isEmpty(req.body.payDate) ? gen.payDate : req.body.payDate,
        gen.payForms = isEmpty(req.body.payForms) ? gen.payForms : req.body.payForms,
        lateEarly = isEmpty(req.body.lateEarly) ? gen.lateEarly : req.body.lateEarly,
        ovtHour = isEmpty(req.body.ovtHour) ? gen.ovtHour : req.body.ovtHour,
        ovtHourInDateOff = isEmpty(req.body.ovtHourInDateOff) ? gen.ovtHourInDateOff : req.body.ovtHourInDateOff,
        ovtHourInPubHoli = isEmpty(req.body.ovtHourInPubHoli) ? gen.ovtHourInPubHoli : req.body.ovtHourInPubHoli,
        workInDateOff = isEmpty(req.body.workInDateOff) ? gen.workInDateOff : req.body.workInDateOff,
        workInPubHoli = isEmpty(req.body.workInPubHoli) ? gen.workInPubHoli : req.body.workInPubHoli,
        nightShift = isEmpty(req.body.nightShift) ? gen.nightShift : req.body.nightShift,
        nightShiftInDateOff = isEmpty(req.body.nightShiftInDateOff) ? gen.nightShiftInDateOff : req.body.nightShiftInDateOff,
        nightShiftInPubHoli = isEmpty(req.body.nightShiftInPubHoli) ? gen.nightShiftInPubHoli : req.body.nightShiftInPubHoli,
        gen.dateModified = now;

        gen.save().then(gen => res.status(200).json({ message: "Cập nhật thành công!" }));
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });

});

router.get('/', auth, (req, res) => {
    Gen.find({}).then(gens => {
        res.send(gens[0]);
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

module.exports = router;