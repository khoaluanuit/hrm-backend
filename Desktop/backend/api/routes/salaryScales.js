const express = require("express");
const router = express.Router();
const isEmpty = require("is-empty");

// Get validate salaryScale module
const ValidateSalaScalInput = require("../validation/salaryScale");

const authQlns = require("../middleware/auth-qlns");
const authQlns1 = require("../middleware/auth-qlns");

// Get salaryScale model
const SalaScal = require("../models/salaryScale");

router.post('/create', authQlns, (req, res) => {
    const { errors, isValid } = ValidateSalaScalInput(req.body);
    if (!isValid) return res.status(404).json(errors);
    now = new Date;

    SalaScal.findOne({ idSalaScal: req.body.idSalaScal }).then(sala => {
        if (sala) return res.status(400).json({ "message": "Mã bậc lương đã tồn tại!" });
        const newSalaScal = new SalaScal({
            idSalaScal: req.body.idSalaScal,
            name: req.body.name,
            salary: req.body.salary,
            allowance: req.body.allowance,
            dateCreate: now
        });
        newSalaScal.save()
            .then(sala => res.status(200).json({ "message": "Tạo thành công!" }));
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.get('/', authQlns1, (req, res) => {
    SalaScal.find({}).then(scals => {
        if (scals.length < 1) return res.status(200).json({ "message": "Chưa có bậc lương nào được tạo!" });
        res.send(scals);
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.get('/:id', authQlns1, (req, res) => {
    SalaScal.findById(req.params.id).then(sala => {
        res.send(sala);
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/edit/:id', authQlns, (req, res) => {
    SalaScal.findOne({ idSalaScal: req.body.idSalaScal }).then(sala1 => {
        SalaScal.findById(req.params.id).then(sala => {
            if (!sala) return res.status(404).json({ "message": "Bậc lương không tồn tại!" });
            if (sala1 && sala1.idSalaScal != sala.idSalaScal) {
                return res.status(404).json({ "message": "Mã bậc lương đã tồn tại!" });
            }
            req.body.idSalaScal = isEmpty(req.body.idSalaScal) ? sala.idSalaScal : req.body.idSalaScal;
            req.body.name = isEmpty(req.body.name) ? sala.name : req.body.name;
            req.body.salary = isEmpty(req.body.salary) ? sala.salary : req.body.salary;
            req.body.allowance = isEmpty(req.body.allowance) ? sala.allowance : req.body.allowance;

            const { errors, isValid } = ValidateSalaScalInput(req.body);
            if (!isValid) return res.status(404).json(errors);

            const nowUpdate = new Date();
            sala.idSalaScal = req.body.idSalaScal;
            sala.name = req.body.name;
            sala.salary = req.body.salary;
            sala.allowance = req.body.allowance;
            sala.dateModified = nowUpdate;
            sala.save().then(sala => res.status(200).json({ "message": "Chỉnh sửa thành công!" }));
        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.delete('/delete/:id', authQlns, (req, res) => {
    SalaScal.findByIdAndDelete(req.params.id).then((sala) => {
      res.status(200).json({ message: "Delete successfully!" });
    })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });

module.exports = router;