const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const isEmpty = require("is-empty");
const fs = require('fs');
const _ = require('lodash');
const exec = require('child_process').exec;
const path = require('path');


// Load backup model
const Tkp = require("../models/timekeeping");
const Pr = require("../models/payroll");
const Acc = require("../models/account");

const ValidateTkpInput = require("../validation/timekeepings");


router.post('/salaryCal', (req, res) => {
    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) return res.status(404).json(errors);

    let month = req.body.month;
    let year = req.body.year;
    let payrolls = [];

    Tkp.find({ month, year }).then(tkps => {
        for(let tkp of tkps){
            let lateEarly = 0;
            let onWorkingDays = 0;
            let days = 0;
            for(let i = 0; i < tkp.days.length; i ++){
                if (tkp.days[i].leave.permit === "None"){
                    onWorkingDays++;
                    lateEarly = lateEarly + tkp.days[i].lateEarly;
                }
                if (tkp.days[i].daysOfW != 6 && tkp.days[i].daysOfW != 0) days++;
            }
            payrolls.push({"emp": tkp.idEmp, days, onWorkingDays, lateEarly});
        }
        res.send(payrolls);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });


});



module.exports = router;