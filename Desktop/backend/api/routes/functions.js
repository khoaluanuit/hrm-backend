const express = require("express");
const router = express.Router();

const authQlpq = require("../middleware/auth-qlpq");

const Func = require("../models/function");

router.get('/', authQlpq, (req, res) => {
    Func.find({}).then(funcs => {
        res.send(funcs);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
});

module.exports = router;