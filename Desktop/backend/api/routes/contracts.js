const express = require("express");
const router = express.Router();
const isEmpty = require("is-empty");
const jwt = require("jsonwebtoken");

// Load auth middleware
const authQlns = require("../middleware/auth-qlns");
const authQlns1 = require("../middleware/auth-qlns1");
const auth = require("../middleware/auth");
const authNoSession = require("../middleware/auth-noSession");

// Load contract model
const Contr = require("../models/contract");
// Load employee model
const Emp = require("../models/employee");

// Load input validate
const validateContrInput = require("../validation/contract");

router.post('/create', authQlns1, (req, res) => {
    const { errors, isValid } = validateContrInput(req.body);
    if (!isValid) return res.status(400).json(errors);

    Contr.findOne({ idContr: req.body.idContr }).then(contr => {
        if (contr) return res.status(400).json({ idContr: "Mã hợp đồng đã tồn tại!" });
        else {
            const now = new Date();
            const newContr = new Contr({
                idContr: req.body.idContr,
                type: req.body.type,
                startDate: req.body.startDate,
                endDate: req.body.endDate,
                workingH: req.body.workingH,
                baseSala: req.body.baseSala,
                allowance: req.body.allowance,
                payDate: req.body.payDate,
                payForms: req.body.payForms,
                insurance: req.body.insurance,
                note: req.body.note,
                dateCreated: now,
                dateModified: now
            });
            newContr
                .save()
                .then(contr => res.json(contr))
                .catch(err => console.log(err));
        }
    })
});
router.get('/', authNoSession, (req, res) => {
    let contrsList = [];
    Contr.find({}).then(result => {
        if (result.length < 1) {
            return res
                .status(200)
                .json({ message: "Không có hợp đồng nào!" });
        } else {
            for (let contr of result) {
                contrsList.push({
                    id: contr._id,
                    idContr: contr.idContr,
                    type: contr.type,
                    startDate: contr.startDate,
                    endDate: contr.endDate,
                    workingH: contr.workingH,
                    baseSala: contr.baseSala,
                    allowance: contr.allowance,
                    payDate: contr.payDate,
                    payForms: contr.payForms,
                    insurance: contr.insurance,
                    note: contr.note,
                    dateCreated: contr.dateCreated,
                    dateModified: contr.dateModified
                });
            }
            res.send(contrsList);
        }

    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
router.get("/:id", authQlns1, (req, res) => {
    let contrsList = [];
    Contr.findById(req.params.id).then(contr => {
        contrsList.push({
            idContr: contr.idContr,
            type: contr.type,
            startDate: contr.startDate,
            endDate: contr.endDate,
            workingH: contr.workingH,
            baseSala: contr.baseSala,
            allowance: contr.allowance,
            payDate: contr.payDate,
            payForms: contr.payForms,
            insurance: contr.insurance,
            note: contr.note,
            dateCreated: contr.dateCreated, dateModified: contr.dateModified
        });
        res.send(contrsList);
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.get("/employee/contract", auth, (req, res) => {
    let contrsList = [];
    const enToken = req.header('x-auth-token');
    const token = enToken.slice(0,30) + enToken.slice(30,50).split("").reverse().join("") + enToken.slice(50);
    const decoded = jwt.verify(token, process.env.JWT);
    Emp.findOne({ account: decoded.email }).then(emp => {
        if (!emp) return res.status(400).json({ contract: "Thông tin của bạn chưa được cập nhật!" });
        else {
            Contr.findOne({ idContr: emp.contract }).then(contr => {
                if (!contr) return res.status(400).json({ contract: "Hợp đồng của bạn chưa được cập nhật!" });
                else {
                    contrsList.push({
                        idContr: contr.idContr,
                        type: contr.type,
                        startDate: contr.startDate,
                        endDate: contr.endDate,
                        workingH: contr.workingH,
                        baseSala: contr.baseSala,
                        allowance: contr.allowance,
                        payDate: contr.payDate,
                        payForms: contr.payForms,
                        insurance: contr.insurance,
                        note: contr.note,
                    });
                    res.send(contrsList);
                }
            });
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/edit/:id', authQlns1, (req, res) => {
    Contr.findOne({ idContr: req.body.idContr }).then(contr => {
        if (contr) return res.status(400).json({ idContr: "Mã hợp đồng đã tồn tại!" });
        else {
            Contr.findById(req.params.id).then(contr => {
                if (!contr) return res.status(200).json({ updatePosi: "Không tìm thấy hợp đồng!" });
                else {
                    req.body.type = isEmpty(req.body.type) ? contr.type : req.body.type;
                    req.body.idContr = isEmpty(req.body.idContr) ? contr.idContr : req.body.idContr;
                    req.body.startDate = isEmpty(req.body.startDate) ? contr.startDate : req.body.startDate;
                    req.body.endDate = isEmpty(req.body.endDate) ? contr.endDate : req.body.endDate;
                    req.body.workingH = isEmpty(req.body.workingH) ? contr.workingH : req.body.workingH;
                    req.body.baseSala = isEmpty(req.body.baseSala) ? contr.baseSala : req.body.baseSala;
                    req.body.allowance = isEmpty(req.body.allowance) ? contr.allowance : req.body.allowance;
                    req.body.payDate = isEmpty(req.body.payDate) ? contr.payDate : req.body.payDate;
                    req.body.payForms = isEmpty(req.body.payForms) ? contr.payForms : req.body.payForms;
                    req.body.insurance = isEmpty(req.body.insurance) ? contr.insurance : req.body.insurance;
                    req.body.note = isEmpty(req.body.note) ? contr.note : req.body.note;

                    // Form validation
                    const { errors, isValid } = validateContrInput(req.body);
                    // Check validation
                    if (!isValid) {
                        return res.status(400).json(errors);
                    }
                    let nowUpdate = new Date();
                    contr.type = req.body.type;
                    contr.idContr = req.body.idContr;
                    contr.startDate = req.body.startDate;
                    contr.endDate = req.body.endDate;
                    contr.workingH = req.body.workingH;
                    contr.baseSala = req.body.baseSala;
                    contr.allowance = req.body.allowance;
                    contr.payDate = req.body.payDate;
                    contr.payForms = req.body.payForms;
                    contr.insurance = req.body.insurance;
                    contr.note = req.body.note;
                    contr.dateModified = nowUpdate;
                    contr
                        .save()
                        .then(contr => res.json(contr));
                }
            })
                .catch(err => {
                    console.log(err);
                    res.status(500).json({
                        error: err
                    });
                });
        }
    });

});

router.delete('/delete/:id', authQlns, (req, res) => {
    Contr.findByIdAndDelete(req.params.id).then((contr) => {
        res.status(200).json({ deletePosition: "Xóa thành công!" });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

module.exports = router;