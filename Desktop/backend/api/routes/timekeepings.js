const express = require("express");
const router = express.Router();
const isEmpty = require("is-empty");
const jwt = require("jsonwebtoken");

// Load auth middleware
const auth = require("../middleware/auth");
const authQlns = require("../middleware/auth-qlns");
const authQlns1 = require("../middleware/auth-qlns1");

// Load department model
const Tkp = require("../models/timekeeping");
const Emp = require("../models/employee");

const ValidateTkpInput = require("../validation/timekeepings");
const ValidateTkp1Input = require("../validation/timekeepings1");


exports.createTKP = (idEmp, month, year) => {
    const firstDateInMonth = year + "-" + month + "-" + "1";
    const now = new Date(firstDateInMonth);
    now.setHours(now.getHours() + 7);
    let days = [];
    let day = now.getDay();
    switch (Number(month)) {
        case 11:
            for (let i = 1; i < 31; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        case 12:
            for (let i = 1; i < 32; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        case 1:
            for (let i = 1; i < 32; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        case 2:
            let maxDays = 28;
            if ((year % 4 === 0 && year % 100 != 0) || year % 4000 === 0) maxDays = 29;
            for (let i = 1; i < maxDays + 1; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        case 3:
            for (let i = 1; i < 32; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        case 4:
            for (let i = 1; i < 31; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        case 5:
            for (let i = 1; i < 32; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        case 6:
            for (let i = 1; i < 31; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        case 7:
            for (let i = 1; i < 32; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        case 8:
            for (let i = 1; i < 32; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        case 9:
            for (let i = 1; i < 31; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        case 10:
            for (let i = 1; i < 32; i++) {
                days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                        "permit": false,
                        "reason": "None",
                        "approver": "None",
                        "note": "None",
                        "empBackup": "None",
                        "contact": "None",
                        "dateOfProcessing": "None",
                        "dateCreate": "None"
                    }
                });
                if (day === 9) day = 2;
            }
            break;
        default:
            return console.log("Vui lòng chọn lại tháng!");
    }
    Emp.findOne({ idEmp }).then(emp => {
        const newTkp = new Tkp({
            idEmp: emp.idEmp,
            month: month,
            year: year,
            days: days,
            annualLeave: 0,
            pubHoli: 0,
            leavePermit: 0,
            leaveNoPermit: 0,
            sickLeave: 0,
            mertanityLeave: 0,
            ovtHour: 0,
            ovtHourInDateOff: 0,
            ovtHourInPubHli: 0,
            workInDateOff: 0,
            workInPubHoli: 0,
            nightShift: 0,
            nightShiftInDateOff: 0,
            nightShiftInPubHoli: 0,
        });
        newTkp.save();
    })
        .catch(err => {
            console.log(err);
        });
}


// Create tkps for all employee in one month
router.post('/create', (req, res) => {
    Tkp.find({}).then(tkps => {
        timenow = new Date();
        year = timenow.getFullYear();
        month = 0;
        for (let tkp of tkps) {
            if (tkp.year > year) year = tkp.year;
        }
        for (let tkp of tkps) {
            if (tkp.year == year && tkp.month > month) month = tkp.month;
        }
        if (month == 12) { year++ , month = 1 }
        else month++;
        Emp.find({}).then(emps => {
            Tkp.findOne({ month, year }).then(tkp => {
                if (tkp) return res.status(400).json({ Warning: "Bảng chấm công tháng " + month + "\/" + year + " đã tồn tại!" });
                else {
                    for (emp of emps) {
                        this.createTKP(emp.idEmp, month, year);   
                    }
                }
                res.status(200).json({ Message: "Bảng chấm công tháng " + month + "\/" + year + " đã được tạo thành công!" });
            });

        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

// Create a tkp for one employee with month, year, idEmp
router.post('/create/employee', (req, res) => { //authQlns
    Tkp.find({}).then(tkps => {
        const idEmp = req.body.idEmp;
        timenow = new Date();
        year = timenow.getFullYear();
        month = 0;
        for (let tkp of tkps) {
            if (tkp.year > year) year = tkp.year;
        }
        for (let tkp of tkps) {
            if (tkp.year == year && tkp.month > month) month = tkp.month;
        }

        if (isEmpty(idEmp)) return res.status(200).json({ message: "Vui lòng chọn nhân viên!" });
        let monthNow = timenow.getMonth();
        let yearNow = timenow.getFullYear();
        try {
            while (yearNow < year) {
                monthNow++;
                this.createTKP(idEmp, monthNow, yearNow);
                if (monthNow == 12) {
                    monthNow = 0;
                    yearNow ++;
                }
            }
            while (yearNow == year && monthNow <= month) {
                this.createTKP(idEmp, monthNow, yearNow);
                if (monthNow == 12) {
                    monthNow = 0;
                    yearNow ++;
                }
                monthNow++;
            }
        } catch (e) {
            console.log(e);
        }
    });
});

// Get tkps list with month
router.get('/months', auth, (req, res) => {
    let months = [];
    Tkp.find({}).then(tkps => {
        if (tkps.length < 1) return res.status(200).json({ message: "Chưa có bảng chấm công nào được tạo!" });
        else {
            for (tkp of tkps) {
                var findMonth = months.find(function (month) {
                    return month === tkp.month + "\/" + tkp.year;
                });
                if (!findMonth) months.push(tkp.month + "\/" + tkp.year);
            }
            res.send(months);
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/monthsWithId', authQlns1, (req, res) => {
    let months = [];
    idEmp = req.body.idEmp;
    Tkp.find({}).then(tkps => {
        if (tkps.length < 1) return res.status(200).json({ message: "Chưa có bảng chấm công nào được tạo!" });
        else {
            for (tkp of tkps) {
                var findMonth = months.find(function (month) {
                    return month === tkp.month + "\/" + tkp.year;
                });
                if (!findMonth && tkp.idEmp === idEmp) months.push(tkp.month + "\/" + tkp.year);
            }
            res.send(months);
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/month', authQlns1, (req, res) => {

    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) return res.status(404).json(errors);

    month = req.body.month;
    year = req.body.year;
    now = new Date();
    console.log(now);
    Tkp.find({ month, year }).then(tkps => {
        if (tkps.length < 1) return res.status(200).json({ message: "Chưa có bảng chấm công nào được tạo!" });
        else {
            if ((year == now.getFullYear() && month < now.getMonth() + 1) || (year < now.getFullYear())) {
                for (let tkp of tkps) {
                    let leaveNotPermitYet = 0;
                    let days = [];
                    for (let i = 0; i < tkp.days.length; i++) {
                        days.push({
                            "date": tkp.days[i].date,
                            "daysOfW": tkp.days[i].daysOfW,
                            "checkIn": tkp.days[i].checkIn,
                            "checkOut": tkp.days[i].checkOut,
                            "lateEarly": tkp.days[i].lateEarly,
                            "leave": {
                                "permit": tkp.days[i].leave.permit,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "note": tkp.days[i].leave.note,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                            }
                        });
                        if (days[i].daysOfW == 6 || days[i].daysOfW == 0) {
                            days[i].leave.permit = "Off";
                            days[i].leave.reason = "None";
                            days[i].leave.approver = "None";
                            days[i].leave.note = "None";
                            days[i].checkIn = "None";
                            days[i].checkOut = "None";
                        }
                        else if (days[i].checkIn != "None" && days[i].checkOut != "None") {
                            days[i].leave.permit = "None"; // tương ứng với có đi làm
                            days[i].leave.reason = "None";
                            days[i].leave.approver = "None";
                            days[i].leave.note = "None";
                        }
                        else if (days[i].leave.permit === false && days[i].leave.reason === "None") {
                            days[i].leave.note = "Nghỉ không phép.";
                            leaveNotPermitYet++;
                        }
                        else if (days[i].permit === false && days[i].leave.reason != "None"
                            && days[i].leave.approver === "None") {
                            days[i].leave.note = "Nghỉ không phép (Chưa duyệt).";
                            leaveNotPermitYet++;
                        }
                        else if (days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver != "None") {
                            leaveNotPermitYet++;
                        }
                    }
                    tkp.leaveNoPermit = leaveNotPermitYet;
                    tkp.days = days;
                    tkp.save();
                }
            } else if (year == now.getFullYear() && month == now.getMonth() + 1) {
                for (let tkp of tkps) {
                    let leaveNotPermitYet = 0;
                    let days = [];
                    for (let i = 0; i < tkp.days.length; i++) {
                        days.push({
                            "date": tkp.days[i].date,
                            "daysOfW": tkp.days[i].daysOfW,
                            "checkIn": tkp.days[i].checkIn,
                            "checkOut": tkp.days[i].checkOut,
                            "lateEarly": tkp.days[i].lateEarly,
                            "leave": {
                                "permit": tkp.days[i].leave.permit,
                                "reason": tkp.days[i].leave.reason,
                                "approver": tkp.days[i].leave.approver,
                                "note": tkp.days[i].leave.note,
                                "dateCreate": tkp.days[i].leave.dateCreate,
                                "empBackup": tkp.days[i].leave.empBackup,
                                "contact": tkp.days[i].leave.contact,
                                "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                            }
                        });
                        if (days[i].daysOfW == 6 || days[i].daysOfW == 0) {
                            days[i].leave.permit = "Off";
                            days[i].leave.reason = "None";
                            days[i].leave.approver = "None";
                            days[i].leave.note = "None";
                            days[i].checkIn = "None";
                            days[i].checkOut = "None";
                        }
                        else if (days[i].checkIn != "None" && days[i].checkOut != "None") {
                            days[i].leave.permit = "None"; // tương ứng với có đi làm
                            days[i].leave.reason = "None";
                            days[i].leave.approver = "None";
                            days[i].leave.note = "None";
                        }
                        else if (days[i].date < now.getDate() && days[i].leave.permit === false
                            && days[i].leave.reason === "None") {
                            days[i].leave.note = "Nghỉ không phép.";
                            leaveNotPermitYet++;
                        }
                        else if (days[i].date < now.getDate() && days[i].permit === false && days[i].leave.reason != "None"
                            && days[i].leave.approver === "None") {
                            days[i].leave.note = "Nghỉ không phép (Chưa duyệt)."
                            leaveNotPermitYet++;
                        }
                        else if (days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver != "None") {
                            leaveNotPermitYet++;
                        }
                    }
                    tkp.leaveNoPermit = leaveNotPermitYet;
                    tkp.days = days;
                    tkp.save();
                }
            }
            res.send(tkps);
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

// Get tkp with idEmp
router.post('/month/employee', auth, (req, res) => {

    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) return res.status(404).json(errors);

    month = req.body.month;
    year = req.body.year;
    idEmp = req.body.idEmp;

    now = new Date();
    console.log(now);

    if (isEmpty(idEmp)) return res.status(200).json({ message: "Vui lòng chọn nhân viên!" });
    Tkp.findOne({ idEmp, month, year }).then(tkp => {

        if (!tkp) return res.status(200).json({ message: "Không tìm thấy bảng chấm công này!" });
        else {
            Emp.findOne({ idEmp }).then(emp => {
                let days = [];
                for (let i = 0; i < tkp.days.length; i++) {
                    days.push({
                        "date": tkp.days[i].date,
                        "daysOfW": tkp.days[i].daysOfW,
                        "checkIn": tkp.days[i].checkIn,
                        "checkOut": tkp.days[i].checkOut,
                        "lateEarly": tkp.days[i].lateEarly,
                        "leave": {
                            "permit": tkp.days[i].leave.permit,
                            "reason": tkp.days[i].leave.reason,
                            "approver": tkp.days[i].leave.approver,
                            "note": tkp.days[i].leave.note,
                            "dateCreate": tkp.days[i].leave.dateCreate,
                            "empBackup": tkp.days[i].leave.empBackup,
                            "contact": tkp.days[i].leave.contact,
                            "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                        }
                    });
                    if (days[i].checkIn != "None" && days[i].checkOut != "None") {
                        days[i].leave.permit = "None"; // tương ứng với có đi làm
                        days[i].leave.reason = "None";
                        days[i].leave.approver = "None";
                        days[i].leave.note = "None";
                    }
                }
                let leaveNotPermitYet = 0;
                let lateEarly = 0;
                if ((year == now.getFullYear() && month < now.getMonth() + 1) || (year < now.getFullYear())) {
                    for (let i = 0; i < tkp.days.length; i++) {
                        if (days[i].daysOfW == 6 || days[i].daysOfW == 7) {
                            days[i].leave.permit = "Off";
                            days[i].leave.reason = "None";
                            days[i].leave.approver = "None";
                            days[i].leave.note = "None";
                            days[i].checkIn = "None";
                            days[i].checkOut = "None";
                        }
                        else if (days[i].leave.permit === false && days[i].leave.reason === "None") {
                            days[i].leave.note = "Nghỉ không phép.";
                            leaveNotPermitYet++;
                        }
                        else if (days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver === "None") {
                            days[i].leave.note = "Nghỉ không phép (Chưa duyệt).";
                            leaveNotPermitYet++;
                        }
                        else if (days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver != "None") {
                            leaveNotPermitYet++;
                        }
                        lateEarly = lateEarly + days[i].leave.lateEarly;
                    }
                    tkp.leaveNoPermit = leaveNotPermitYet;
                    tkp.days = days;
                    tkp.save();
                } else if (year == now.getFullYear() && month == now.getMonth() + 1) {
                    for (let i = 0; i < tkp.days.length; i++) {
                        if (days[i].daysOfW == 6 || days[i].daysOfW == 7) {
                            days[i].leave.permit = "Off";
                            days[i].leave.reason = "None";
                            days[i].leave.approver = "None";
                            days[i].leave.note = "None";
                            days[i].checkIn = "None";
                            days[i].checkOut = "None";
                        }
                        else if (days[i].date < now.getDate() && days[i].leave.permit === false && days[i].leave.reason === "None") {
                            days[i].leave.note = "Nghỉ không phép.";
                            leaveNotPermitYet++;
                        }
                        else if (days[i].date < now.getDate() && days[i].leave.permit === false
                            && days[i].leave.reason != "None" && days[i].leave.approver === "None") {
                            days[i].leave.note = "Nghỉ không phép (Chưa duyệt).";
                            leaveNotPermitYet++;
                        }
                        else if (days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver != "None") {
                            leaveNotPermitYet++;
                        }
                        lateEarly = lateEarly + days[i].lateEarly;
                    }
                    if (lateEarly > 0) {
                        tkp.ovtHour = lateEarly;
                        lateEarly = 0;
                    }
                    tkp.leaveNoPermit = leaveNotPermitYet;
                    tkp.days = days;
                    tkp.save();
                }
                tkp1 = {
                    "lateEarly": lateEarly,
                    "nameEmp": emp.fullname,
                    "days": tkp.days,
                    "idEmp": tkp.idEmp,
                    "month": tkp.month,
                    "year": tkp.year,
                    "annualLeave": tkp.annualLeave,
                    "pubHoli": tkp.pubHoli,
                    "leavePermit": tkp.leavePermit,
                    "leaveNoPermit": tkp.leaveNoPermit,
                    "sickLeave": tkp.sickLeave,
                    "mertanityLeave": tkp.mertanityLeave,
                    "ovtHour": tkp.ovtHour,
                    "ovtHourInDateOff": tkp.ovtHourInDateOff,
                    "ovtHourInPubHli": tkp.ovtHourInPubHli,
                    "workInDateOff": tkp.workInDateOff,
                    "workInPubHoli": tkp.workInPubHoli,
                    "nightShift": tkp.nightShift,
                    "nightShiftInDateOff": tkp.nightShiftInDateOff,
                    "nightShiftInPubHoli": tkp.nightShiftInPubHoli,
                };
                res.send(tkp1);
            });
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/month/yourtimekeeping', auth, (req, res) => {

    month = req.body.month;
    year = req.body.year;

    const enToken = req.header('x-auth-token');
    const token = enToken.slice(0,30) + enToken.slice(30,50).split("").reverse().join("") + enToken.slice(50);
    const decoded = jwt.verify(token, process.env.JWT);

    now = new Date();
    console.log(now);
    Emp.findOne({ account: decoded.email }).then(emp => {
        console.log(emp);
        if (!emp) return res.status(400).json({ Message: "Dữ liệu của bạn chưa được cập nhật!" });
        Tkp.findOne({ idEmp: emp.idEmp, month, year }).then(tkp => {
            if (!tkp) return res.status(400).json({ Message: "Không tìm thấy bảng chấm công này!" });
            else {
                let days = [];
                for (let i = 0; i < tkp.days.length; i++) {
                    days.push({
                        "date": tkp.days[i].date,
                        "daysOfW": tkp.days[i].daysOfW,
                        "checkIn": tkp.days[i].checkIn,
                        "checkOut": tkp.days[i].checkOut,
                        "lateEarly": tkp.days[i].lateEarly,
                        "leave": {
                            "permit": tkp.days[i].leave.permit,
                            "reason": tkp.days[i].leave.reason,
                            "approver": tkp.days[i].leave.approver,
                            "note": tkp.days[i].leave.note,
                            "dateCreate": tkp.days[i].leave.dateCreate,
                            "empBackup": tkp.days[i].leave.empBackup,
                            "contact": tkp.days[i].leave.contact,
                            "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                        }
                    });
                    if (days[i].checkIn != "None" && days[i].checkOut != "None") {
                        days[i].leave.permit = "None"; // tương ứng với có đi làm
                        days[i].leave.reason = "None";
                        days[i].leave.approver = "None";
                        days[i].leave.note = "None";
                    }
                }
                let leaveNotPermitYet = 0;
                if ((year == now.getFullYear() && month < now.getMonth() + 1) || (year < now.getFullYear())) {
                    for (let i = 0; i < tkp.days.length; i++) {
                        if (days[i].daysOfW == 6 || days[i].daysOfW == 7) {
                            days[i].leave.permit = "Off";
                            days[i].leave.reason = "None";
                            days[i].leave.approver = "None";
                            days[i].leave.note = "None";
                            days[i].checkIn = "None";
                            days[i].checkOut = "None";
                        }
                        else if (days[i].leave.permit === false && days[i].leave.reason === "None") {
                            days[i].leave.note = "Nghỉ không phép.";
                            leaveNotPermitYet++;
                        }
                        else if (days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver === "None") {
                            days[i].leave.note = "Nghỉ không phép (Chưa duyệt).";
                            leaveNotPermitYet++;
                        }
                        else if (days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver != "None") {
                            leaveNotPermitYet++;
                        }
                    }
                    tkp.leaveNoPermit = leaveNotPermitYet;
                    tkp.days = days;
                    tkp.save();
                } else if (year == now.getFullYear() && month == now.getMonth() + 1) {
                    for (let i = 0; i < tkp.days.length; i++) {
                        if (days[i].daysOfW == 6 || days[i].daysOfW == 7) {
                            days[i].leave.permit = "Off";
                            days[i].leave.reason = "None";
                            days[i].leave.approver = "None";
                            days[i].leave.note = "None";
                            days[i].checkIn = "None";
                            days[i].checkOut = "None";
                        }
                        else if (days[i].date < now.getDate() && days[i].leave.permit === false && days[i].leave.reason === "None") {
                            days[i].leave.note = "Nghỉ không phép.";
                            leaveNotPermitYet++;
                        }
                        else if (days[i].date < now.getDate() && days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver === "None") {
                            days[i].leave.note = "Nghỉ không phép (Chưa duyệt).";
                            leaveNotPermitYet++;
                        }
                        else if (days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver != "None") {
                            leaveNotPermitYet++;
                        }
                    }
                    tkp.leaveNoPermit = leaveNotPermitYet;
                    tkp.days = days;
                    tkp.save();
                }
                res.send(tkp);
            }
        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

// tkp function
router.post('/edit', authQlns1, (req, res) => {

    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) return res.status(404).json(errors);
    const { errors1, isValid1 } = ValidateTkp1Input(req.body);
    if (!isValid1) return res.status(404).json(errors1);

    month = req.body.month;
    year = req.body.year;
    idEmp = req.body.idEmp;
    daysIn = req.body.days;
    // cần truyền vào đối tượng days như bên dưới
    // days = [
    // { 
    //    "date" : req.body.date,
    //   "checkIn": req.body.checkIn,
    //   "checkOut": req.body.checkOut,
    //   "lateEarly": req.body.lateEarly
    // },
    // { 
    //    "date" : req.body.date1,
    //   "checkIn": req.body.checkIn,
    //   "checkOut": req.body.checkOut,
    //   "lateEarly": req.body.lateEarly
    // }
    // .
    // .
    // .
    // ]

    now = new Date();

    if (isEmpty(idEmp)) return res.status(200).json({ message: "Vui lòng chọn nhân viên!" });
    Tkp.findOne({ idEmp, month, year }).then(tkp => {
        if (!tkp) return res.status(200).json({ message: "Không tìm thấy bảng chấm công này!" });
        else {
            let days = [];
            // test dataIn
            //let daysIn = [
            //    {
            //       "date" : req.body.date1 - 1,
            //        "checkIn": req.body.checkIn1,
            //        "checkOut": req.body.checkOut1,
            //       "lateEarly": req.body.lateEarly1
            //    },
            //    {
            //        "date" : req.body.date2 - 1,
            //        "checkIn": req.body.checkIn2,
            //        "checkOut": req.body.checkOut2,
            //        "lateEarly": req.body.lateEarly2
            //   }
            //];
            for (let i = 0; i < tkp.days.length; i++) {
                days.push({
                    "date": tkp.days[i].date,
                    "daysOfW": tkp.days[i].daysOfW,
                    "checkIn": tkp.days[i].checkIn,
                    "checkOut": tkp.days[i].checkOut,
                    "lateEarly": tkp.days[i].lateEarly,
                    "leave": {
                        "permit": tkp.days[i].leave.permit,
                        "reason": tkp.days[i].leave.reason,
                        "approver": tkp.days[i].leave.approver,
                        "note": tkp.days[i].leave.note,
                        "dateCreate": tkp.days[i].leave.dateCreate,
                        "empBackup": tkp.days[i].leave.empBackup,
                        "contact": tkp.days[i].leave.contact,
                        "dateOfProcessing": tkp.days[i].leave.dateOfProcessing,
                    }
                });
            }

            for (let i = 0; i < daysIn.length; i++) {
                days[daysIn[i].date - 1].checkIn = isEmpty(daysIn[i].checkIn) ? tkp.days[i].checkIn : daysIn[i].checkIn;
                days[daysIn[i].date - 1].checkOut = isEmpty(daysIn[i].checkOut) ? tkp.days[i].checkOut : daysIn[i].checkOut;
                days[daysIn[i].date - 1].lateEarly = isEmpty(daysIn[i].lateEarly) ? tkp.days[i].lateEarly : daysIn[i].lateEarly;

                if (days[daysIn[i].date - 1].checkIn != "None" && days[daysIn[i].date].checkOut != "None") {
                    days[daysIn[i].date - 1].leave.permit = "None"; // tương ứng với có đi làm
                    days[daysIn[i].date - 1].leave.reason = "None";
                    days[daysIn[i].date - 1].leave.approver = "None";
                    days[daysIn[i].date - 1].leave.note = "None";
                }
            }
            let leaveNotPermitYet = 0;
            if ((year == now.getFullYear() && month < now.getMonth() + 1) || (year < now.getFullYear())) {
                for (let i = 0; i < tkp.days.length; i++) {
                    if (days[i].daysOfW == 6 || days[i].daysOfW == 0) {
                        days[i].leave.permit = "Off";
                        days[i].leave.reason = "None";
                        days[i].leave.approver = "None";
                        days[i].leave.note = "None";
                        days[i].checkIn = "None";
                        days[i].checkOut = "None";
                    }
                    else if (days[i].leave.permit === false && days[i].leave.reason === "None") {
                        days[i].leave.note = "Nghỉ không phép.";
                        leaveNotPermitYet++;
                    }
                    else if (days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver === "None") {
                        days[i].leave.note = "Nghỉ không phép (Chưa duyệt).";
                        leaveNotPermitYet++;
                    }
                    else if (days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver != "None") {
                        leaveNotPermitYet++;
                    }
                }
                tkp.leaveNoPermit = leaveNotPermitYet;
                tkp.days = days;
            } else if (year == now.getFullYear() && month == now.getMonth() + 1) {
                for (let i = 0; i < tkp.days.length; i++) {
                    if (days[i].daysOfW == 6 || days[i].daysOfW == 0) {
                        days[i].leave.permit = "Off";
                        days[i].leave.reason = "None";
                        days[i].leave.approver = "None";
                        days[i].leave.note = "None";
                        days[i].checkIn = "None";
                        days[i].checkOut = "None";
                    }
                    else if (days[i].date < now.getDate() && days[i].leave.permit === false && days[i].leave.reason === "None") {
                        days[i].leave.note = "Nghỉ không phép.";
                        leaveNotPermitYet++;
                    }
                    else if (days[i].date < now.getDate() && days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver === "None") {
                        days[i].leave.note = "Nghỉ không phép (Chưa duyệt).";
                        leaveNotPermitYet++;
                    }
                    else if (days[i].permit === false && days[i].leave.reason != "None" && days[i].leave.approver != "None") {
                        leaveNotPermitYet++;
                    }
                }
                tkp.leaveNoPermit = leaveNotPermitYet;
                tkp.days = days;
            } else {
                return res.status(404).json({ message: "Bạn chỉ có thể chỉnh sửa bảng chấm công hiện tại trở về trước!" });
            }

            tkp.pubHoli = isEmpty(req.body.pubHoli) ? tkp.pubHoli : req.body.pubHoli;
            tkp.annualLeave = isEmpty(req.body.annualLeave) ? tkp.annualLeave : req.body.annualLeave;
            tkp.sickLeave = isEmpty(req.body.sickLeave) ? tkp.sickLeave : req.body.sickLeave;
            tkp.mertanityLeave = isEmpty(req.body.mertanityLeave) ? tkp.mertanityLeave : req.body.mertanityLeave;
            tkp.ovtHour = isEmpty(req.body.ovtHour) ? tkp.ovtHour : req.body.ovtHour;
            tkp.ovtHourInDateOff = isEmpty(req.body.ovtHourInDateOff) ? tkp.ovtHourInDateOff : req.body.ovtHourInDateOff;
            tkp.ovtHourInPubHli = isEmpty(req.body.ovtHourInPubHli) ? tkp.ovtHourInPubHli : req.body.ovtHourInPubHli;
            tkp.workInDateOff = isEmpty(req.body.workInDateOff) ? tkp.workInDateOff : req.body.workInDateOff;
            tkp.workInPubHoli = isEmpty(req.body.workInPubHoli) ? tkp.workInPubHoli : req.body.workInPubHoli;
            tkp.nightShift = isEmpty(req.body.nightShift) ? tkp.nightShift : req.body.nightShift;
            tkp.nightShiftInDateOff = isEmpty(req.body.nightShiftInDateOff) ? tkp.nightShiftInDateOff : req.body.nightShiftInDateOff;
            tkp.nightShiftInPubHoli = isEmpty(req.body.nightShiftInPubHoli) ? tkp.nightShiftInPubHoli : req.body.nightShiftInPubHoli;

            tkp
                .save()
                .then(tkp => res.send(tkp));

        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.delete('/delete/employee', authQlns, (req, res) => {

    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) return res.status(404).json(errors);

    month = req.body.month;
    year = req.body.year;
    idEmp = req.body.idEmp;

    if (isEmpty(idEmp)) return res.status(200).json({ message: "Vui lòng chọn nhân viên!" });
    Tkp.findOneAndDelete({ idEmp, month, year }).then(tkp => {
        res.status(200).json({ message: "Xóa thành công!" });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.delete('/delete', authQlns, (req, res) => {

    const { errors, isValid } = ValidateTkpInput(req.body);
    if (!isValid) return res.status(404).json(errors);

    month = req.body.month;
    year = req.body.year;

    Tkp.find({ month, year }).then(tkps => {
        if (tkps.length < 1) res.status(200).json({ message: "Không tồn tại bảng chấm công này!" });
        else {
            for (let tkp of tkps) {
                tkp.delete();
            }
            res.status(200).json({ message: "Xóa thành công!" });
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });

});

module.exports = router;