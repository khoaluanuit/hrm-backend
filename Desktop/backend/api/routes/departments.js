const express = require("express");
const router = express.Router();
const isEmpty = require("is-empty");

// Load auth middleware
const authQlns = require("../middleware/auth-qlns");
const authQlns1 = require("../middleware/auth-qlns1");
const authNoSession = require("../middleware/auth-noSession");
const authTbtk = require("../middleware/auth-tbtk");

// Load department model
const Depart = require("../models/department");

// Load input validate
const validateDepartInput = require("../validation/department");

// Create department
router.post('/create', authQlns1, (req, res) => {
    const { errors, isValid } = validateDepartInput(req.body);
    if (!isValid) return res.status(400).json(errors);

    Depart.findOne({ idDpmt: req.body.idDpmt }).then(depart => {
        if (depart) return res.status(400).json({ Warning: "Mã phòng ban đã tồn tại!" });
        Depart.findOne({ email: req.body.email }).then(depart => {
            if (depart && depart.email != "being updated") return res.status(400).json({ email: "Email đã tồn tại!" });
            else {
                const now = new Date();
                const newDepart = new Depart({
                    idDpmt: req.body.idDpmt,
                    name: req.body.name,
                    email: req.body.email,
                    manager: req.body.manager,
                    dateCreated: now,
                    dateModified: now
                });
                newDepart
                    .save()
                    .then(depart => res.json(depart))
                    .catch(err => console.log(err));
            }
        });
    })
});
router.get('/', authNoSession, (req, res) => {
    Depart.find({}).then(departs => {
        if (departs.length < 1) {
            return res
                .status(200)
                .json({ message: "Chưa phòng ban nào được tạo!" });
        } else res.send(departs);
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.get("/:id", authQlns1, (req, res) => {
    let departsList = [];
    Depart.findById(req.params.id).then(depart => {
        departsList.push({ id: depart._id, name: depart.name, email: depart.email, manager: depart.manager });
        res.send(departsList);
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/edit/:id', authQlns1, (req, res) => {
    Depart.findOne({ idDpmt: req.body.idDpmt }).then(depart1 => {
        Depart.findOne({ email: req.body.email }).then(depart2 => {
            Depart.findById(req.params.id).then(depart => {
                if (depart2 && depart2.email != "being updated" && depart2.email != depart.email) {
                    return res.status(400).json({ email: "Email đã tồn tại." });
                }
                if (depart1 && depart1.idDpmt != depart.idDpmt) return res.status(400).json({ Warning: "Mã phòng ban đã tồn tại!" });
                if (!depart) return res.status(200).json({ updateuser: "Không tìm thấy phòng ban." });
                else {
                    req.body.name = isEmpty(req.body.name) ? depart.name : req.body.name;
                    req.body.email = isEmpty(req.body.email) ? depart.email : req.body.email;
                    req.body.manager = isEmpty(req.body.manager) ? depart.manager : req.body.manager;
                    req.body.idDpmt = isEmpty(req.body.idDpmt) ? depart.idDpmt : req.body.idDpmt;
                    // Form validation
                    const { errors, isValid } = validateDepartInput(req.body);
                    // Check validation
                    if (!isValid) {
                        return res.status(400).json(errors);
                    }
                    let nowUpdate = new Date();
                    depart.name = req.body.name;
                    depart.email = req.body.email;
                    depart.manager = req.body.manager;
                    depart.idDpmt = req.body.idDpmt;
                    depart.dateModified = nowUpdate;
                    depart
                        .save()
                        .then(depart => res.json(depart));
                }
            });
        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });

});

router.delete('/delete/:id', authQlns, (req, res) => {
    Depart.findByIdAndDelete(req.params.id).then((depart) => {
        res.status(200).json({ Message: "Xóa thành công!" });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

module.exports = router;