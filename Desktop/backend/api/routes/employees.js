const express = require("express");
const router = express.Router();
const isEmpty = require("is-empty");
const jwt = require("jsonwebtoken");

// Load auth middlewre
const auth = require("../middleware/auth");
const authQlns = require("../middleware/auth-qlns");
const authQlns1 = require("../middleware/auth-qlns1");
const authTbtkNoSession = require("../middleware/auth-tbtk-noSession");
const authNoSession = require("../middleware/auth-noSession");

// Load employee model
const Emp = require("../models/employee");
const Posi = require("../models/position");

// Load input validate
const validateEmpInput = require("../validation/employee");

// Create empment
router.post('/create', authQlns1, (req, res) => {
    const { errors, isValid } = validateEmpInput(req.body);
    if (!isValid) return res.status(400).json(errors);

    Emp.findOne({ idEmp: req.body.idEmp }).then(emp => {
        if (emp) return res.status(400).json({ idEmp: "Mã nhân viên đã tồn tại!" });
        Emp.findOne({ account: req.body.account }).then(emp => {
            if (emp) return res.status(400).json({ acc: "Tài khoản đã tồn tại!" });
        });
        Emp.findOne({ email: req.body.email }).then(emp => {
            if (emp) return res.status(400).json({ email: "Email đã tồn tại!" });
            else {
                const now = new Date()
                const newEmp = new Emp({
                    idEmp: req.body.idEmp,
                    fullname: req.body.fullname,
                    email: req.body.email,
                    account: req.body.account,
                    contract: req.body.contract,
                    gender: req.body.gender,
                    birthdate: req.body.birthdate,
                    birthplace: req.body.birthplace,
                    placeOfRes: req.body.placeOfRes,
                    address: req.body.address,
                    phone: req.body.phone,
                    idCard: req.body.idCard,
                    dateOfIssue: req.body.dateOfIssue,
                    placeOfIssue: req.body.placeOfIssue,
                    bSalarySca: req.body.bSalarySca,
                    religion: req.body.religion,
                    position: req.body.position,
                    standard: req.body.standard,
                    nationlaty: req.body.nationlaty,
                    maritalStt: req.body.maritalStt,
                    department: req.body.department,
                    startDate: req.body.startDate,
                    image: req.body.image,
                    note: req.body.note,
                    dateCreated: now,
                    dateModified: now
                });
                newEmp
                    .save()
                    .then(emp => res.json(emp))
                    .catch(err => console.log(err));
            }
        });
    })
});
router.get('/', authTbtkNoSession, (req, res) => {
    let empsList = [];
    Emp.find({}).then(result => {
        if (result.length < 1) {
            return res
                .status(200)
                .json({ message: "Chưa có nhân viên nào!" });
        } else {
            for (let emp of result) {
                empsList.push({
                    id: emp._id,
                    idEmp: emp.idEmp,
                    fullname: emp.fullname,
                    email: emp.email,
                    account: emp.account,
                    contract: emp.contract,
                    gender: emp.gender,
                    birthdate: emp.birthdate,
                    birthplace: emp.birthplace,
                    placeOfRes: emp.placeOfRes,
                    address: emp.address,
                    phone: emp.phone,
                    idCard: emp.idCard,
                    dateOfIssue: emp.dateOfIssue,
                    placeOfIssue: emp.placeOfIssue,
                    bSalarySca: emp.bSalarySca,
                    religion: emp.religion,
                    position: emp.position,
                    standard: emp.standard,
                    nationlaty: emp.nationlaty,
                    maritalStt: emp.maritalStt,
                    department: emp.department,
                    startDate: emp.startDate,
                    image: emp.image,
                    note: emp.note,
                    dateCreated: emp.dateCreated,
                    dateModified: emp.dateModified
                });
            }
            res.send(empsList);
        }

    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.get('/nomalUser', authNoSession, (req, res) => {
    let empsList = [];
    Emp.find({}).then(result => {
        if (result.length < 1) {
            return res
                .status(200)
                .json({ message: "Chưa có nhân viên nào!" });
        } else {
            for (let emp of result) {
                empsList.push({
                    idEmp: emp.idEmp,
                    fullname: emp.fullname,
                    email: emp.email,
                    phone: emp.phone,
                    position: emp.position,
                    department: emp.department,
                    placeOfRes: emp.placeOfRes,
                    address: emp.address,
                    note: emp.note
                });
            }
            res.send(empsList);
        }

    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.get("/:id", auth, (req, res) => {
    let empsList = [];
    Emp.findById(req.params.id).then(emp => {
        empsList.push({
            id: emp._id,
            idEmp: emp.idEmp,
            fullname: emp.fullname,
            email: emp.email,
            account: emp.account,
            contract: emp.contract,
            gender: emp.gender,
            birthdate: emp.birthdate,
            birthplace: emp.birthplace,
            placeOfRes: emp.placeOfRes,
            address: emp.address,
            phone: emp.phone,
            idCard: emp.idCard,
            dateOfIssue: emp.dateOfIssue,
            placeOfIssue: emp.placeOfIssue,
            bSalarySca: emp.bSalarySca,
            religion: emp.religion,
            position: emp.position,
            standard: emp.standard,
            nationlaty: emp.nationlaty,
            maritalStt: emp.maritalStt,
            department: emp.department,
            startDate: emp.startDate,
            image: emp.image,
            note: emp.note,
            dateCreated: emp.dateCreated,
            dateModified: emp.dateModified
        });
        res.send(empsList);
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.get("/employee/yourinfo", authNoSession, (req, res) => {
    let empsList = [];
    const enToken = req.header('x-auth-token');
    const token = enToken.slice(0,30) + enToken.slice(30,50).split("").reverse().join("") + enToken.slice(50);
    const decoded = jwt.verify(token, process.env.JWT);
    Emp.findOne({ account: decoded.email }).then(emp => {
        if (!emp) return res.status(400).json({ employee: "Thông tin của bạn chưa được cập nhật!" });
        else {
            Posi.findOne({ name: emp.position }).then(posi => {
                if (!posi) {
                    empsList.push({
                        id: emp._id,
                        idEmp: emp.idEmp,
                        fullname: emp.fullname,
                        email: emp.email,
                        account: emp.account,
                        role: decoded.role,
                        contract: emp.contract,
                        gender: emp.gender,
                        birthdate: emp.birthdate,
                        birthplace: emp.birthplace,
                        placeOfRes: emp.placeOfRes,
                        address: emp.address,
                        phone: emp.phone,
                        idCard: emp.idCard,
                        dateOfIssue: emp.dateOfIssue,
                        placeOfIssue: emp.placeOfIssue,
                        bSalarySca: emp.bSalarySca,
                        religion: emp.religion,
                        position: emp.position,
                        idPosi: "Chưa được cập nhật hoặc đã bị cập nhật sai.",
                        standard: emp.standard,
                        nationlaty: emp.nationlaty,
                        maritalStt: emp.maritalStt,
                        department: emp.department,
                        startDate: emp.department,
                        image: emp.image,
                        note: emp.note,
                    });
                }
                else {
                    empsList.push({
                        id: emp._id,
                        idEmp: emp.idEmp,
                        fullname: emp.fullname,
                        email: emp.email,
                        account: emp.account,
                        role: decoded.role,
                        contract: emp.contract,
                        gender: emp.gender,
                        birthdate: emp.birthdate,
                        birthplace: emp.birthplace,
                        placeOfRes: emp.placeOfRes,
                        address: emp.address,
                        phone: emp.phone,
                        idCard: emp.idCard,
                        dateOfIssue: emp.dateOfIssue,
                        placeOfIssue: emp.placeOfIssue,
                        bSalarySca: emp.bSalarySca,
                        religion: emp.religion,
                        position: emp.position,
                        idPosi: posi.idPosi,
                        standard: emp.standard,
                        nationlaty: emp.nationlaty,
                        maritalStt: emp.maritalStt,
                        department: emp.department,
                        startDate: emp.department,
                        image: emp.image,
                        note: emp.note,
                    });
                }
                res.send(empsList);
            });
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});
router.post('/edit/:id', authQlns1, (req, res) => {
    try {
        Emp.findOne({ idEmp: req.body.idEmp }).then(emp1 => {
            Emp.findOne({ email: req.body.email }).then(emp2 => {
                Emp.findById(req.params.id).then(emp => {
                    if (!emp) return res.status(200).json({ messager: "Không tìm thấy nhân viên." });
                    if (emp2 && emp2.email != emp.email) return res.status(400).json({ email: "Email đã tồn tại." });
                    if (emp1 && emp1.idEmp != emp.idEmp) return res.status(400).json({ idDpmt: "Mã nhân viên đã tồn tại." });
                    else {
                        req.body.fullname = isEmpty(req.body.fullname) ? emp.fullname : req.body.fullname;
                        req.body.email = isEmpty(req.body.email) ? emp.email : req.body.email;
                        req.body.account = isEmpty(req.body.account) ? emp.account : req.body.account;
                        req.body.idEmp = isEmpty(req.body.idEmp) ? emp.idEmp : req.body.idEmp;
                        req.body.contract = isEmpty(req.body.contract) ? emp.contract : req.body.contract;
                        req.body.gender = isEmpty(req.body.gender) ? emp.gender : req.body.gender;
                        req.body.birthdate = isEmpty(req.body.birthdate) ? emp.birthdate : req.body.birthdate;
                        req.body.birthplace = isEmpty(req.body.birthplace) ? emp.birthplace : req.body.birthplace;
                        req.body.placeOfRes = isEmpty(req.body.placeOfRes) ? emp.placeOfRes : req.body.placeOfRes;
                        req.body.address = isEmpty(req.body.address) ? emp.address : req.body.address;
                        req.body.phone = isEmpty(req.body.phone) ? emp.phone : req.body.phone;
                        req.body.idCard = isEmpty(req.body.idCard) ? emp.idCard : req.body.idCard;
                        req.body.dateOfIssue = isEmpty(req.body.dateOfIssue) ? emp.dateOfIssue : req.body.dateOfIssue;
                        req.body.placeOfIssue = isEmpty(req.body.placeOfIssue) ? emp.placeOfIssue : req.body.placeOfIssue;
                        req.body.bSalarySca = isEmpty(req.body.bSalarySca) ? emp.ibSalarySca : req.body.bSalarySca;
                        req.body.religion = isEmpty(req.body.religion) ? emp.ireligion : req.body.religion;
                        req.body.position = isEmpty(req.body.position) ? emp.iposition : req.body.position;
                        req.body.standard = isEmpty(req.body.standard) ? emp.istandard : req.body.standard;
                        req.body.nationlaty = isEmpty(req.body.nationlaty) ? emp.inationlaty : req.body.nationlaty;
                        req.body.maritalStt = isEmpty(req.body.maritalStt) ? emp.imaritalStt : req.body.maritalStt;
                        req.body.department = isEmpty(req.body.department) ? emp.idepartment : req.body.department;
                        req.body.startDate = isEmpty(req.body.startDate) ? emp.istartDate : req.body.startDate;
                        req.body.image = isEmpty(req.body.image) ? emp.idmage : req.body.image;
                        req.body.note = isEmpty(req.body.note) ? emp.note : req.body.note;
                        // Form validation
                        const { errors, isValid } = validateEmpInput(req.body);
                        // Check validation
                        if (!isValid) {
                            return res.status(400).json(errors);
                        }
                        let nowUpdate = new Date();
                        emp.idEmp = req.body.idEmp,
                            emp.fullname = req.body.fullname,
                            emp.email = req.body.email,
                            emp.account = req.body.account,
                            emp.contract = req.body.contract,
                            emp.gender = req.body.gender,
                            emp.birthdate = req.body.birthdate,
                            emp.birthplace = req.body.birthplace,
                            emp.placeOfRes = req.body.placeOfRes,
                            emp.address = req.body.address,
                            emp.phone = req.body.phone,
                            emp.idCard = req.body.idCard,
                            emp.dateOfIssue = req.body.dateOfIssue,
                            emp.placeOfIssue = req.body.placeOfIssue,
                            emp.bSalarySca = req.body.bSalarySca,
                            emp.religion = req.body.religion,
                            emp.position = req.body.position,
                            emp.standard = req.body.standard,
                            emp.nationlaty = req.body.nationlaty,
                            emp.maritalStt = req.body.maritalStt,
                            emp.department = req.body.department,
                            emp.startDate = req.body.startDate,
                            emp.image = req.body.image,
                            emp.note = req.body.note,
                            emp.dateModified = nowUpdate;
                        emp
                            .save()
                            .then(emp => res.send(emp));
                    }
                });
            });
        });
    } catch (e) {
        console.log(err);
            res.status(500).json({
                Error: "Vui lòng reload lại trang!"
            });
    }
});

router.post('/edit/employee/yourinfo', auth, (req, res) => {
    const enToken = req.header('x-auth-token');
    const token = enToken.slice(0,30) + enToken.slice(30,50).split("").reverse().join("") + enToken.slice(50);
    const decoded = jwt.verify(token, process.env.JWT);
    Emp.findOne({ account: decoded.email }).then(emp => {
        Emp.findOne({ email: req.body.email }).then(emp2 => {
            if (!emp) return res.status(200).json({ messager: "Không tìm thấy nhân viên." });
            if (emp2 && emp2.email != emp.email) return res.status(400).json({ email: "Email đã tồn tại." });
            else {
                req.body.fullname = isEmpty(req.body.fullname) ? emp.fullname : req.body.fullname;
                    req.body.email = isEmpty(req.body.email) ? emp.email : req.body.email;
                    req.body.account = emp.account;
                    req.body.idEmp = emp.idEmp;
                    req.body.contract = emp.contract;
                    req.body.gender = isEmpty(req.body.gender) ? emp.gender : req.body.gender;
                    req.body.birthdate = isEmpty(req.body.birthdate) ? emp.birthdate : req.body.birthdate;
                    req.body.birthplace = isEmpty(req.body.birthplace) ? emp.birthplace : req.body.birthplace;
                    req.body.placeOfRes = isEmpty(req.body.placeOfRes) ? emp.placeOfRes : req.body.placeOfRes;
                    req.body.address = isEmpty(req.body.address) ? emp.address : req.body.address;
                    req.body.phone = isEmpty(req.body.phone) ? emp.phone : req.body.phone;
                    req.body.idCard = isEmpty(req.body.idCard) ? emp.idCard : req.body.idCard;
                    req.body.dateOfIssue = isEmpty(req.body.dateOfIssue) ? emp.dateOfIssue : req.body.dateOfIssue;
                    req.body.placeOfIssue = isEmpty(req.body.placeOfIssue) ? emp.placeOfIssue : req.body.placeOfIssue;
                    req.body.bSalarySca = emp.ibSalarySca;
                    req.body.religion = isEmpty(req.body.religion) ? emp.ireligion : req.body.religion;
                    req.body.position = emp.iposition;
                    req.body.standard = isEmpty(req.body.standard) ? emp.istandard : req.body.standard;
                    req.body.nationlaty = isEmpty(req.body.nationlaty) ? emp.inationlaty : req.body.nationlaty;
                    req.body.maritalStt = isEmpty(req.body.maritalStt) ? emp.imaritalStt : req.body.maritalStt;
                    req.body.department =  emp.idepartment;
                    req.body.startDate = isEmpty(req.body.startDate) ? emp.istartDate : req.body.startDate;
                    req.body.image = isEmpty(req.body.image) ? emp.idmage : req.body.image;
                    req.body.note = isEmpty(req.body.note) ? emp.note : req.body.note;
                // Form validation
                const { errors, isValid } = validateEmpInput(req.body);
                // Check validation
                if (!isValid) {
                    return res.status(400).json(errors);
                }
                let nowUpdate = new Date();
                emp.idEmp = req.body.idEmp,
                    emp.fullname = req.body.fullname,
                    emp.email = req.body.email,
                    emp.gender = req.body.gender,
                    emp.birthdate = req.body.birthdate,
                    emp.birthplace = req.body.birthplace,
                    emp.placeOfRes = req.body.placeOfRes,
                    emp.address = req.body.address,
                    emp.phone = req.body.phone,
                    emp.idCard = req.body.idCard,
                    emp.dateOfIssue = req.body.dateOfIssue,
                    emp.placeOfIssue = req.body.placeOfIssue,
                    emp.religion = req.body.religion,
                    emp.standard = req.body.standard,
                    emp.nationlaty = req.body.nationlaty,
                    emp.maritalStt = req.body.maritalStt,
                    emp.startDate = req.body.startDate,
                    emp.image = req.body.image,
                    emp.note = req.body.note,
                    emp.dateModified = nowUpdate;
                emp
                    .save()
                    .then(emp => res.json(emp));
            }
        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });

});

router.delete('/delete/:id', authQlns, (req, res) => {
    Emp.findByIdAndDelete(req.params.id).then((emp) => {
        res.status(200).json({ deleteEmploy: "Xóa thành công!" });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

module.exports = router;