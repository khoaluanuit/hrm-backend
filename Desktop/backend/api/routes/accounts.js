const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const isEmpty = require("is-empty");
const mailer = require("./mailer");
const ranPass = require("secure-random-password");



// Load account model
const Acc = require("../models/account");
const Role = require("../models/role");
const Func = require("../models/function");

// Load employee model
const Emp = require("../models/employee");
// Load auth middleware
const auth = require("../middleware/auth");
const authNoSession = require("../middleware/auth-noSession");
const authQlht1 = require("../middleware/auth-qlht1");
const authQlht = require("../middleware/auth-qlht");
// Load input validation
const validateRegisterInput = require("../validation/register");
const validateLoginInput = require("../validation/login");
const validatePasswordInput = require("../validation/password");

exports.encrypt = (str) => {
    return str.slice(0,30) + str.slice(30,50).split("").reverse().join("") + str.slice(50);
}
exports.decrypt = (str) => {
  return str.slice(0,60) + str.slice(60,80).split("").reverse().join("") + str.slice(80);
}
exports.getIdEmp = (callback) => {
  var n = Math.floor(Math.random() * 10000);
  var m = n;
  var count = 0;
  if (m >= 1)++count;
  while (m / 10 >= 1) {
    m /= 10;
    ++count;
  }
  if (count == 5) n = "VNG-" + n;
  else if (count == 4) n = "VNG-0" + n;
  else if (count == 3) n = "VNG-00" + n;
  else if (count == 2) n = "VNG-000" + n;
  else if (count == 1) n = "VNG-0000" + n;
  Emp.findOne({ 'idEmp': n }, function (err, result) {
    if (err) callback(err);
    else if (result) {
      return getIdEmp(callback);
    }
    else {
      callback(null, n);
    }
  });
}

exports.getOTP = (callback) => {
  var n = ranPass.randomPassword({ length: 10, characters: [ranPass.lower, ranPass.upper, ranPass.digits] });
  Acc.findOne({ 'lastOTP': n }, function (err, result) {
    if (err) callback(err);
    else if (result) return getOTP(callback);
    else callback(null, n);
  });
}

router.post("/register", (req, res) => { //authQlht1
  // Form validation
  const { errors, isValid } = validateRegisterInput(req.body);
  const { errors1, isValid1 } = validatePasswordInput(req.body);
  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  if (!isValid1) {
    return res.status(400).json(errors1);
  }
  Acc.findOne({ email: req.body.email }).then(user => {
    if (user) {
      return res.status(400).json({ email: "Email đã tồn tại!" });
    } else {
      const enToken = req.header('x-auth-token');
      const token = this.encrypt(enToken);
      const decoded = jwt.verify(token, process.env.JWT);
      if (!isEmpty(req.body.role) && decoded.role != "adHt" && decoded.role != "suAd") {
        return res.status(400).json({ role: "Bạn không được phép thực hiện hành động này!" });
      }
      if (req.body.role == "suAd" && decoded.role != "suAd") {
        return res.status(400).json({ role: "Bạn không được phép thực hiện hành động này!" });
      }
      const now = new Date();
      const newAcc = new Acc({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        active: true,
        role: isEmpty(req.body.role) ? "nv" : req.body.role,
        auth2: false,
        isLogin: 0,
        numberOfReset : 0,
        lastOTP : "None",
        timeOTP : 0,
        numberOfLoginFail : 0,
        timeLoginFail : 0,
        dateCreated: now,
        dateModified: now
      });
      // Hash password before saving in database
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newAcc.password, salt, (err, hash) => {
          if (err) throw err;
          newAcc.password = hash;
          newAcc
            .save()
            .then(user => res.json(user))
            .catch(err => console.log(err));
        });
      });
      mailer.sendMail(req.body.email, "TÀI KHOẢN ĐĂNG NHẬP VÀO HỆ THỐNG HRM2019!", "<strong>email: </strong>" + req.body.email + "<br>  <strong>password: </strong>" + req.body.password);

      this.getIdEmp(function (error, idEmp) {
        const newEmp = new Emp({
          idEmp: idEmp,
          fullname: req.body.username,
          account: req.body.email,
          dateCreated: now,
          dateModified: now
        });
        newEmp.save();

        Tkp.find({}).then(tkps => {
          let timenow = new Date();
          let yearMax = timenow.getFullYear();
          let monthMax = 0;
          for (let tkp of tkps) {
            if (tkp.year > yearMax) yearMax = tkp.year;
          }
          for (let tkp of tkps) {
            if (tkp.year == yearMax && tkp.month > monthMax) monthMax = tkp.month;
          }

          let month = timenow.getMonth();
          let year = timenow.getFullYear();
          while (year < yearMax) {
            month++;
            const firstDateInMonth = year + "-" + month + "-" + "1";
            const now = new Date(firstDateInMonth);
            now.setHours(now.getHours() + 7);
            let days = [];
            let day = now.getDay();

            switch (Number(month)) {
              case 11:
                for (let i = 1; i < 31; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 12:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 1:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 2:
                let maxDays = 28;
                if ((year % 4 === 0 && year % 100 != 0) || year % 4000 === 0) maxDays = 29;
                for (let i = 1; i < maxDays + 1; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 3:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 4:
                for (let i = 1; i < 31; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 5:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 6:
                for (let i = 1; i < 31; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 7:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 8:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 9:
                for (let i = 1; i < 31; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 10:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              default:
                return console.log("Vui lòng chọn lại tháng!");
            }
            Emp.findOne({ idEmp }).then(emp => {
              const newTkp = new Tkp({
                idEmp: emp.idEmp,
                month: month,
                year: year,
                days: days,
                annualLeave: 0,
                pubHoli: 0,
                leavePermit: 0,
                leaveNoPermit: 0,
                sickLeave: 0,
                mertanityLeave: 0,
                ovtHour: 0,
                ovtHourInDateOff: 0,
                ovtHourInPubHli: 0,
                workInDateOff: 0,
                workInPubHoli: 0,
                nightShift: 0,
                nightShiftInDateOff: 0,
                nightShiftInPubHoli: 0,
              });
              newTkp.save();
            })
              .catch(err => {
                console.log(err);
              });
            if (month == 12) {
              month = 0;
              year++;
            }
          }
          while (year == yearMax && month <= monthMax) {
            ++month;
            const firstDateInMonth = year + "-" + month + "-" + "1";
            const now = new Date(firstDateInMonth);
            now.setHours(now.getHours() + 7);
            let days = [];
            let day = now.getDay();
            console.log("abc" + day);
            switch (Number(month)) {
              case 11:
                for (let i = 1; i < 31; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 12:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 1:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 2:
                let maxDays = 28;
                if ((year % 4 === 0 && year % 100 != 0) || year % 4000 === 0) maxDays = 29;
                for (let i = 1; i < maxDays + 1; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 3:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 4:
                for (let i = 1; i < 31; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 5:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 6:
                for (let i = 1; i < 31; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 7:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 8:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 9:
                for (let i = 1; i < 31; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              case 10:
                for (let i = 1; i < 32; i++) {
                  days.push({
                    "date": i,
                    "daysOfW": day++,
                    "checkIn": "None",
                    "checkOut": "None",
                    "lateEarly": 0,
                    "leave": {
                      "permit": false,
                      "reason": "None",
                      "approver": "None",
                      "note": "None",
                      "empBackup": "None",
                      "contact": "None",
                      "dateOfProcessing": "None",
                      "dateCreate": "None"
                    }
                  });
                  if (day === 9) day = 2;
                }
                break;
              default:
                return console.log("Vui lòng chọn lại tháng!");
            }

            Emp.findOne({ account: req.body.email }).then(emp => {
              if (month > 1) {
                month--;
                const newTkp = new Tkp({
                  idEmp: emp.idEmp,
                  month: month,
                  year: year,
                  days: days,
                  annualLeave: 0,
                  pubHoli: 0,
                  leavePermit: 0,
                  leaveNoPermit: 0,
                  sickLeave: 0,
                  mertanityLeave: 0,
                  ovtHour: 0,
                  ovtHourInDateOff: 0,
                  ovtHourInPubHli: 0,
                  workInDateOff: 0,
                  workInPubHoli: 0,
                  nightShift: 0,
                  nightShiftInDateOff: 0,
                  nightShiftInPubHoli: 0,
                });
                newTkp.save();
              }
            })
              .catch(err => {
                console.log(err);
              });

            if (month == 12) {
              month = 0;
              year++;
            }
          }
        });

      });
    }
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.post("/login", (req, res) => {
  // Form validation
  const { errors, isValid } = validateLoginInput(req.body);
  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  const email = req.body.email;
  const password = req.body.password;
  let wrong = 0;
  // Find user by email
  Acc.findOne({ email }).then(user => {
    // Check if user exists
    if (!user) {
      let wrong = 1;
      user.numberOfLoginFail = user.numberOfLoginFail + 1;
      if (user.numberOfLoginFail === 1) user.timeLoginFail = Date.now() + 300000;
      else if (Date.now() - user.timeLoginFail > 0) {
        user.timeLoginFail = Date.now() + 300000;
        user.numberOfLoginFail = 0;
      }
      else if (user.numberOfLoginFail > 5 && Date.now() - user.timeLoginFail < 0) user.active = false;
      user.save();
      return res.status(404).json({ Warning: "Không tìm thấy email!" }); //emailNotFound
    } else if (user.active === false) {
      // Check if user is disabled
      return res.status(400).json({ Message: "Tài khoản của bản đã bị tạm ngưng hoạt đông, liên hệ bộ phận quản trị hệ thống để được hỗ trợ." });
    } else if (user.isLogin - Date.now() > 0) {
      return res.status(400).json({ Message: "Tài khoản của bản đang được đăng nhập ở một nơi khác, vui lòng đăng xuất ở nơi đó trước khi có thể đăng nhập tại đây" });
    }
    else {
      bcrypt.compare(password, user.password).then(isMatch => {
        // Check password
        if (isMatch) {
          // User matched
          user.numberOfLoginFail = 0;
          user.save();
          Emp.findOne({ account: user.email }).then(emp => {
            if (!emp) {
              // Create JWT Payload
              const payload = {
                id: user.id,
                expires: Date.now() + 3600000,
                email: user.email,
                role: user.role,
                auth2: user.auth2,
                phone: ""
              };
              // Sign token
              jwt.sign(
                payload,
                process.env.JWT,
                (err, token) => {
                  let enToken = this.decrypt(token);
                  res.json({
                    token: enToken,
                    payload
                  });
                }
              );
            } else {
              // Create JWT Payload
              const payload = {
                id: user.id,
                expires: Date.now() + 3600000,
                email: user.email,
                role: user.role,
                auth2: user.auth2,
                phone: emp.phone
              };
              // Sign token
              jwt.sign(
                payload,
                process.env.JWT,
                (err, token) => {
                  let enToken = this.decrypt(token);
                  console.log(token);
                  res.json({
                    token: enToken,
                    payload
                  });
                }
              );
            }
          });
        } else {
          if (wrong === 0) {
            user.numberOfLoginFail = user.numberOfLoginFail + 1;
            if (user.numberOfLoginFail === 1) user.timeLoginFail = Date.now() + 300000;
            else if (Date.now() - user.timeLoginFail > 0) {
              user.timeLoginFail = Date.now() + 300000;
              user.numberOfLoginFail = 0;
            }
            else if (user.numberOfLoginFail > 5 && Date.now() - user.timeLoginFail < 0) user.active = false;
            user.save();
          }
          return res
            .status(400)
            .json({ Warning: "Kiểm tra lại password" }); //passwordincorrect
        }
      });
    }
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.get('/', authQlht1, (req, res) => {
  const enToken = req.header('x-auth-token');
  const token = this.encrypt(enToken);
  const decoded = jwt.verify(token, process.env.JWT);
  let accslist = [];
  Acc.find({}).then(result => {
    if (result.length < 1) {
      return res
        .status(200)
        .json({ message: "Chưa có người dùng nào được tạo!" });
    }
    else {
      Role.find({}).then(roles => {
        for (let user of result) {
          var findRole = roles.find(function (role) {
            return role.idRo === user.role;
          });
          if (findRole && decoded.role === "adHt" && findRole.idRo != "suAd" && findRole.idRo != "adHt") {
            accslist.push({ id: user._id, username: user.username, email: user.email, active: user.active, role: findRole.name, auth2: user.auth2 });
          } else if (findRole && decoded.role === "nvHt" && findRole.idRo != "suAd"
            && findRole.idRo != "nvHt" && findRole.idRo != "adHt") {
            accslist.push({ id: user._id, username: user.username, email: user.email, active: user.active, role: findRole.name, auth2: user.auth2 });
          } else if (findRole && decoded.role === "suAd") {
            accslist.push({ id: user._id, username: user.username, email: user.email, active: user.active, role: findRole.name, auth2: user.auth2, isLogin: user.isLogin });
          }
        }
        res.send(accslist);
      });
    }

  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.get('/employee', (req, res) => { //authNoSession
  let accslist = [];
  Acc.find({}).then(accs => {
    Emp.find({}).then(emps => {
      for (let acc of accs) {
        var findEmp = emps.find(function (emp) {
          return acc.email === emp.account;
        });
        if (!findEmp) accslist.push(acc.email);
      }
      res.send(accslist);
    });
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.get("/:id", auth, (req, res) => {
  let accslist = [];
  Acc.findById(req.params.id).then(user => {
    accslist.push({ id: user._id, username: user.username, email: user.email });
    res.send(accslist);
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.post('/edit/:id', authQlht1, (req, res) => {

  Acc.findOne({ email: req.body.email }).then(user1 => {
    Acc.findById(req.params.id).then(user => {
      if (!user) return res.status(404).json({ updateUser: "Không tìm thấy user" });
      else {
        if (user1 && user1.email != user.email) return res.status(404).json({ email: "Email đã được sử dụng!" });
        const enToken = req.header('x-auth-token');
        const token = this.encrypt(enToken);
        const decoded = jwt.verify(token, process.env.JWT);
        if (!isEmpty(req.body.role) && decoded.role != "adHt" && decoded.role != "suAd") {
          return res.status(400).json({ role: "Bạn không được phép thực hiện hành động này!" });
        }

        req.body.username = isEmpty(req.body.username) ? user.username : req.body.username;
        req.body.email = isEmpty(req.body.email) ? user.email : req.body.email;
        req.body.active = isEmpty(req.body.active) ? user.active : req.body.active;
        req.body.role = isEmpty(req.body.role) ? user.role : req.body.role;
        req.body.auth2 = isEmpty(req.body.auth2) ? user.auth2 : req.body.auth2;
        // Form validation
        const { errors, isValid } = validateRegisterInput(req.body);
        // Check validation
        if (!isValid) {
          return res.status(400).json(errors);
        }
        const nowUpdate = new Date();
        user.username = req.body.username;
        user.email = req.body.email;
        user.active = req.body.active;
        user.role = req.body.role;
        user.auth2 = req.body.auth2;
        user.dateModified = nowUpdate;
        if (user.active === true) user.timeLoginFail = 0;
        user
          .save()
          .then(user => res.json(user))
          .catch(err => console.log(err));
      }
    });
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.post('/edit/yourinfo/password', auth, (req, res) => {
  const enToken = req.header('x-auth-token');
  const token = this.encrypt(enToken);
  const decoded = jwt.verify(token, process.env.JWT);
  Acc.findOne({ email: decoded.email }).then(user => {
    if (!user) return res.status(404).json({ message: "Không tìm thấy tài khoản!" });
    else {
      if (isEmpty(req.body.passwordOld)) return res.status(404).json({ message: "Vui lòng nhập password hiện tại!" });
      bcrypt.compare(req.body.passwordOld, user.password).then(isMatch => {
        if (isMatch) {
          req.body.password = isEmpty(req.body.password) ? user.password : req.body.password;
          req.body.password2 = isEmpty(req.body.password2) ? user.password : req.body.password2;
          const nowUpdate = new Date();
          user.dateModified = nowUpdate;
          if (user.password === req.body.password) {
            user.password = req.body.password;
            user
              .save()
              .then(user => res.json(user))
              .catch(err => console.log(err));
          }
          else {
            const { errors1, isValid1 } = validatePasswordInput(req.body);
            if (!isValid1) {
              return res.status(400).json(errors1);
            }
            else {
              user.password = req.body.password;
              bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(user.password, salt, (err, hash) => {
                  if (err) throw err;
                  user.password = hash;
                  user
                    .save()
                    .then(user => res.json(user))
                    .catch(err => console.log(err));
                });
              });
            }
          }
        }
        else {
          return res
            .status(400)
            .json({ message: "Password hiện tại không chính xác!" });
        }
      });
    }
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.post('/edit/password/:id', authQlht1, (req, res) => {
  Acc.findById(req.params.id).then(user => {
    if (!user) return res.status(404).json({ message: "Không tìm thấy tài khoản!" });
    else {
      const nowUpdate = new Date();
      user.dateModified = nowUpdate;

      let password = ranPass.randomPassword({ length: 8, characters: [ranPass.lower, ranPass.upper, ranPass.digits] });
      user.password = password;
      user.numberOfReset = 0;
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(user.password, salt, (err, hash) => {
          if (err) throw err;
          user.password = hash;
          user
            .save()
            .then(user => {
              mailer.sendMail(user.email,
                "TÀI KHOẢN ĐĂNG NHẬP VÀO HỆ THỐNG HRM2019!",
                "Mật khẩu đăng nhập vào hệ thống HRM2019 của bạn đã được admin thay đổi, cụ thể như sau: <br> <strong>email: </strong>" + user.email + "<br>  <strong>password: </strong>" + password);
              res.status(200).json({ Message: "Mật khẩu của " + user.email + " đã được reset thành công. Mật khẩu mới là: " + password });
            });
        });
      });
    }
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.post('/verify', (req, res) => {
  let email = req.body.email;
  if (isEmpty(email)) return res.status(400).json({ Message: "Vui lòng điền email tài khoản cần reset password!" });
  Acc.findOne({ email }).then(user => {
    if (!user) return res.status(404).json({ Message: "Không tìm thấy người dùng!" });
    else if (user.numberOfReset > 5) return res.status(404).json({ Message: "Bạn đã reset password nhiều lần, nếu không nhận được mã xác thực qua email tài khoản, vui lòng liên hệ admin để được hổ trợ!" });
    else {
      this.getOTP(function (error, OTP) {
        user.lastOTP = OTP;
        user.numberOfReset = user.numberOfReset + 1;
        user.timeOTP = Date.now() + 300000;
        user.save().then(user => {
          mailer.sendMail(user.email,
            "XÁC NHÂN RESET PASSWORD ĐĂNG NHẬP VÀO HỆ THỐNG HRM2019!",
            "Bạn đã thực hiện reset password vào hệ thống HRM2019, mã xác thực của bạn là:" + "<strong>" + OTP + "</strong>");
          res.status(200).json({ Message: "Mã xác thực đã được gửi về email: " + user.email + " vui lòng điền mã xác thực để tiếp tục!" });
        });
      });
    }
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.post('/resetYourPassword', (req, res) => {
  let lastOTP = req.body.otp;
  let email = req.body.email;
  console.log(lastOTP);
  Acc.findOne({ email }).then(user => {
    if (!user) return res.status(404).json({ message: "Không tìm thấy người dùng!" });
    else if (user.numberOfReset > 10) return res.status(404).json({ message: "Bạn đã xác thực nhiều lần, nếu không nhận được mã xác thực qua email tài khoản, vui lòng liên hệ admin để được hổ trợ!" });
    else if (user.lastOTP != lastOTP || user.lastOTP == "None" || Date.now() - user.timeOTP > 0) {
      user.numberOfReset = user.numberOfReset + 1;
      return res.status(404).json({ message: "Mã xác thực không đúng!" });
    }
    else {
      user.numberOfReset = 0;
      user.lastOTP = "None"
      let password = ranPass.randomPassword({ length: 8, characters: [ranPass.lower, ranPass.upper, ranPass.digits] });
      user.password = password;
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(user.password, salt, (err, hash) => {
          if (err) throw err;
          user.password = hash;
          user
            .save()
            .then(user => {
              mailer.sendMail(user.email,
                "TÀI KHOẢN ĐĂNG NHẬP VÀO HỆ THỐNG HRM2019!",
                "Mật khẩu đăng nhập vào hệ thống HRM2019 của bạn đã được reset thành công, cụ thể như sau: <br> <strong>email: </strong>" + user.email + "<br>  <strong>password: </strong>" + password);
              res.status(200).json({ Message: "Successfully!" });
            });
        });
      });
    }
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.delete('/delete/:id', authQlht, (req, res) => {
  Acc.findById(req.params.id).then(user => {
    Acc.findByIdAndDelete(req.params.id).then(acc => {
      Emp.findOne({ account: user.email }).then(emp => {
        emp.delete();
        Tkp.find({ idEmp: emp.idEmp }).then(tkps => {
          for (let tkp of tkps) {
            tkp.delete();
          }
        });
        res.status(200).json({ deleteuser: "Xóa thành công!" });
      });
    });
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.get('/account/role', authNoSession, (req, res) => {
  const enToken = req.header('x-auth-token');
  const token = this.encrypt(enToken);
  const decoded = jwt.verify(token, process.env.JWT);
  role = decoded.role;
  let roleFunc = [];
  
  Acc.findOne({ email: decoded.email }).then(acc => {
    acc.isLogin = Date.now() + 3600000;
    acc.save();
  });
  Role.findOne({ idRo: role }).then(role => {
    Func.find({}).then(funcst => {
      for (let func of funcst) {
        var findFunc = role.funcs.find(function (funcRole) {
          return funcRole === func.idFunc;
        });
        if (findFunc) roleFunc.push({ idFunc: func.idFunc, name: func.name, description: func.description });
      }
      let funcRole = { role: role.idRo, funcs: roleFunc };
      res.send(funcRole);
    })
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });

});

router.post('/logout', auth, (req, res) => {
  const enToken = req.header('x-auth-token');
  const token = this.encrypt(enToken);
  const decoded = jwt.verify(token, process.env.JWT);

  Acc.findOne({ email: decoded.email }).then(acc => {
    if (!acc) res.status(404).json({ Message: "Không tìm thấy user!" });
    acc.isLogin = 0;
    acc.save();
    res.status(200).json({ Message: "Đăng xuất thành công!" });
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.post('/abc/abc/abc', (req, res) => {
  Acc.find({}).then(accs => {
    for (let acc of accs) {
      acc.isLogin = 0;
      // acc.save();
      acc.numberOfReset = 0;
      acc.lastOTP = "None";
      acc.timeOTP = 0;
      acc.numberOfLoginFail = 0;
      timeLoginFail = 0;
      acc.save();
    }
  })
});

router.post('/session', (req, res) => {
  //   var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

  //   if (!format.test(req.body.password)) {
  //     console.log("false");
  //   } else {
  //     console.log("true");
  //   }
  // var password = ranPass.randomPassword({ length: 8, characters: [ranPass.lower, ranPass.upper, ranPass.digits] });
  // res.send(password)
    const enToken = req.body.token;

    let token = this.decrypt(enToken);

    
    console.log(token);
    const decoded = jwt.verify(token, process.env.JWT);
    let token1 = this.encrypt(token);
    res.send(token1);

});



module.exports = router;