const express = require("express");
const router = express.Router();
const isEmpty = require("is-empty");
const jwt = require("jsonwebtoken");

// Load auth middleware
const authQlpq = require("../middleware/auth-qlpq");
const authQlht1 = require("../middleware/auth-qlht1");

// Load department model
const Role = require("../models/role");

// Load input validate
const validateRoleInput = require("../validation/role");

router.post('/create', authQlpq, (req, res) => {
    const { errors, isValid } = validateRoleInput(req.body);
    if (!isValid) return res.status(400).json(errors);
    Role.findOne({ idRo: req.body.idRo }).then(role => {
        if (role) return res.status(400).json({ message: "Mã vai trò đã tồn tại!" });
        else {
            const now = new Date();
            const newRole = new Role({
                idRo: req.body.idRo,
                name: req.body.name,
                funcs: req.body.funcs, // funcs = [idFunc1, idFunc2, ...]
                dateCreated: now,
                dateModified: now
            });
            newRole
                .save()
                .then(role => res.json(role))
                .catch(err => console.log(err));
        }
    })
});

router.get('/', authQlht1, (req, res) => {
    const enToken = req.header('x-auth-token');
    const token = enToken.slice(0,30) + enToken.slice(30,50).split("").reverse().join("") + enToken.slice(50);
    const decoded = jwt.verify(token, process.env.JWT);
    let rolesList = [];
    Role.find({}).then(roles => {
        if (roles.length < 1) {
            return res
                .status(200)
                .json({ message: "Chưa có vai trò nào được tạo!" });
        } 
        else {
            for (let role of roles){
                if (decoded.role === "adHt" && role.idRo != "suAd" && role.idRo != "adHt" ){
                    rolesList.push(role);
                } else  if (decoded.role === "nvHt" && role.idRo != "suAd" && role.idRo != "nvHt" && role.idRo != "adHt" ){
                    rolesList.push(role); 
                }
                else if (decoded.role === "suAd") rolesList.push(role);
            }
        } 
        res.send(rolesList);
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.get('/:id', authQlpq, (req, res) => {
    let rolesList = [];
    Role.findById(req.params.id).then(role => {
        if (!role) {
            return res
                .status(200)
                .json({ message: "Không tồn tại vai trò này!" });
        } else {
            rolesList.push({ idRo: role.idRo, name: role.name, funcs: role.funcs });
            res.send(rolesList);
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});


router.post('/edit/:id', authQlpq,(req, res) => {
    Role.findOne({ idRo: req.body.idRo }).then(role => {
        if (role) return res.status(400).json({ message: "Mã vai trò đã tồn tại." });
        else {
            Role.findById(req.params.id).then(role => {
                if (!role) return res.status(200).json({ updateRole: "Không tìm thấy chức danh!" });
                else if (role.idRo === "suAd") return res.status(200).json({ updateRole: "Không thể chỉnh sửa vai trò này" });
                else {
                    req.body.name = isEmpty(req.body.name) ? role.name : req.body.name;
                    req.body.idRo = isEmpty(req.body.idRo) ? role.idRo : req.body.idRo;
                    req.body.funcs = isEmpty(req.body.funcs) ? role.funcs : req.body.funcs;

                    let nowUpdate = new Date();
                    role.name = req.body.name;
                    role.idRo = req.body.idRo;
                    role.funcs = req.body.funcs;
                    role.dateModified = nowUpdate;
                    role
                        .save()
                        .then(role => res.json(role));
                }
            })
                .catch(err => {
                    console.log(err);
                    res.status(500).json({
                        error: err
                    });
                });
        }
    });

});

router.delete('/delete/:id', authQlpq, (req, res) => {
    Role.findById(req.params.id).then((role) => {
        if (role.idRo === "suAd") return res.status(200).json({ deleteRole: "Không thể xóa vai trò này" });
        else {
            Role.findByIdAndDelete(req.params.id).then(
                res.status(200).json({ deleteRole: "Delete successfully!" })
            );
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

module.exports = router;