const express = require("express");
const router = express.Router();
const isEmpty = require("is-empty");

// Load auth middleware
const authQlns = require("../middleware/auth-qlns");
const authQlns1 = require("../middleware/auth-qlns1");
const authTbtk = require("../middleware/auth-tbtk");

// Load department model
const Posi = require("../models/position");
const Depart = require("../models/department");

// Load input validate
const validatePosiInput = require("../validation/position");

router.post('/create', authQlns1, (req, res) => {
    const { errors, isValid } = validatePosiInput(req.body);
    if (!isValid) return res.status(400).json(errors);

    Posi.findOne({ idPosi: req.body.idPosi }).then(posi => {
        if (posi) return res.status(400).json({ idPosi: "Mã chức danh đã tồn tại!" });
        else {
            const now = new Date();
            const newPosi = new Posi({
                idPosi: req.body.idPosi,
                name: req.body.name,
                idDpmt: req.body.idDpmt,
                dateCreated: now,
                dateModified: now
            });
            newPosi
                .save()
                .then(posi => res.json(posi))
                .catch(err => console.log(err));
        }
    })
});
router.get('/', authTbtk, (req, res) => {
    let posisList = [];
    Posi.find({}).then(posis => {
        if (posis.length < 1) {
            return res
                .status(200)
                .json({ message: "Chưa có chức vụ nào được tạo!" });
        } else {
            Depart.find({}).then(dpmts => {
                for (let posi of posis){
                    var findDpmt = dpmts.find(function(dpmt){
                        return dpmt.idDpmt === posi.idDpmt;
                    });
                    if (findDpmt){
                        posisList.push({ id: posi._id, idDpmt: posi.idDpmt, 
                        nameDpmt: findDpmt.name, idPosi: posi.idPosi, name: posi.name,
                        dateCreated: posi.dateCreated, dateModified: posi.dateModified });
                    }
                }
                res.send(posisList);
            });
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/departments', authQlns1, (req, res) => {
    let idDpmt = req.body.idDpmt;
    Posi.find({ idDpmt }).then(posis => {
        if (posis.length < 1) {
            return res
                .status(200)
                .json({ message: "Chưa có chức vụ nào được tạo!" });
        }
        else res.send(posis);


    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/departs', authQlns1, (req, res) => {
    let name = req.body.name;
    Depart.findOne({ name }).then(depart => {
        if (!depart)
        Posi.find({ idDpmt: depart.idDpmt }).then(posis => {
            if (posis.length < 1) {
                return res
                    .status(200)
                    .json({ message: "Chưa có chức vụ nào được tạo!" });
            }
            else res.send(posis);
        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.get("/:id", authQlns1, (req, res) => {
    let posisList = [];
    Posi.findById(req.params.id).then(posi => {
        posisList.push({ id: posi._id, idDpmt: posi.idDpmt, idPosi: posi.idPosi, name: posi.name });
        res.send(posisList);
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

router.post('/edit/:id', authQlns1, (req, res) => {
    Posi.findOne({ idPosi: req.body.idPosi }).then(posi => {
        if (posi) return res.status(400).json({ idPosi: "Mã phòng ban đã tồn tại." });
        else {
            Posi.findById(req.params.id).then(posi => {
                if (!posi) return res.status(200).json({ updatePosi: "Không tìm thấy chức danh." });
                else {
                    req.body.name = isEmpty(req.body.name) ? posi.name : req.body.name;
                    req.body.idPosi = isEmpty(req.body.idPosi) ? posi.idPosi : req.body.idPosi;
                    req.body.idDpmt = isEmpty(req.body.idDpmt) ? posi.idDpmt : req.body.idDpmt;
                    // Form validation

                    let nowUpdate = new Date();
                    posi.name = req.body.name;
                    posi.idPosi = req.body.idPosi;
                    posi.idDpmt = req.body.idDpmt;
                    posi.dateModified = nowUpdate;
                    posi
                        .save()
                        .then(posi => res.json(posi));
                }
            })
                .catch(err => {
                    console.log(err);
                    res.status(500).json({
                        error: err
                    });
                });
        }
    });

});

router.delete('/delete/:id', authQlns, (req, res) => {
    Posi.findByIdAndDelete(req.params.id).then((posi) => {
        res.status(200).json({ deletePosition: "Xóa thành công!" });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

module.exports = router;