const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const accRoutes = require('./api/routes/accounts');
const departRoutes = require('./api/routes/departments');
const empRoutes = require('./api/routes/employees');
const contrRoutes = require('./api/routes/contracts');
const posiRoutes = require('./api/routes/positions');
const tkpRoutes = require('./api/routes/timekeepings');
const roleRoutes = require('./api/routes/roles');
const funcRoutes = require('./api/routes/functions');
const leaveRoutes = require('./api/routes/leaves');
const genRoutes = require('./api/routes/generalInfo');
const salaScalRoutes = require('./api/routes/salaryScales');
const backupRestoreRoutes = require('./api/routes/backupRestore');
const payrollRoutes = require('./api/routes/payrolls');

// DB Config
const db = "mongodb://linh:SOh3TbYhx8ypJPxmt1oOfL@localhost:27017/hrm2019";
//mongodb://linh:ngoclinh123@ds233258.mlab.com:33258/hrm
//mongodb://linhtrinh:SOh3TbYhx8ypJPxOlXuEk4J9r@178.128.222.245:27017/hrm2019
mongoose.connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
})
.then(() => console.log("MongoDB successfully connected"))
.catch(err => console.log(err));

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested, Content-Type, Accept, Authorization, x-auth-token"
    );
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
        return res.status(200).json({});
    }
    next();
})

app.use("/accounts", accRoutes);
app.use("/employees", empRoutes);
app.use("/departments", departRoutes);
app.use("/contracts", contrRoutes);
app.use("/positions", posiRoutes);
app.use("/timekeepings", tkpRoutes);
app.use("/roles", roleRoutes);
app.use("/functions", funcRoutes);
app.use("/leaves", leaveRoutes);
app.use("/generalInfo", genRoutes);
app.use("/salaScals", salaScalRoutes);
app.use("/backupRestores", backupRestoreRoutes);
app.use("/payrolls", payrollRoutes);


app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: { message: error.message }
    });
});

module.exports = app;