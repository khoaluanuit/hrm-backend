const https = require('https');
const http = require('http');
const app = require('./app');
const fs = require('fs');

const port = process.env.PORT || 5000;

// const server = https.createServer({
//     key: fs.readFileSync('./sslcert/key.pem'),
//     cert: fs.readFileSync('./sslcert/cert.pem')
//   }, app);

const server = http.createServer(app);
server.listen(port);
